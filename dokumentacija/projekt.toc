\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Primjer u stvarnom \IeC {\v z}ivotu: naftne bu\IeC {\v s}otine}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Algoritam Bayesove optimizacije}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Gaussovi procesi}{2}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Osnovno}{2}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Parametri Gaussovog procesa}{4}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Modeliranje podataka Gaussovim procesom}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Poja\IeC {\v s}njenje vizualizacije}{7}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}Primjer izra\IeC {\v c}una}{8}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Algoritam Bayesove minimizacije}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Hiperparametri modela}{14}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Vektor o\IeC {\v c}ekivanja Gaussovog procesa}{14}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Kovarijacijska funkcija Gaussovog procesa}{14}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Funkcija akvizicije}{14}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Algoritam optimizacije funkcije akvizicije}{14}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}Primjena na minimizaciju funkcije}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Jednostavan primjer}{15}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Gradijentni spust}{15}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Bayesova optimizacija}{16}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Kombinacija gradijentnog spusta i Bayesove optimizacije}{16}{subsection.3.1.3}
\contentsline {chapter}{\numberline {4}Primjena u strojnom u\IeC {\v c}enju}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Optimizacija hiperparametara neuronske mre\IeC {\v z}e}{17}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Primjer 1}{17}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Primjer 2}{18}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Klasifikacija Gaussovim procesom}{19}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Primjer 3}{19}{subsection.4.2.1}
\contentsline {chapter}{\numberline {5}Zaklju\IeC {\v c}ak}{20}{chapter.5}
\contentsline {chapter}{\numberline {6}Sa\IeC {\v z}etak}{21}{chapter.6}
