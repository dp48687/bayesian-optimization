\documentclass[times, utf8]{fer}

\usepackage{cite}
\usepackage{fullpage}
\usepackage{enumerate}
\usepackage{tocloft}
\usepackage{url}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage[chapter]{algorithm}
\usepackage{algorithmic}
\usepackage[export]{adjustbox}
\usepackage{amssymb}
\usepackage{pgfplots}

\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}

\graphicspath{ {./images/} }

\pgfplotsset{width=7cm,compat=1.8}
\pgfplotsset{
  colormap={whitered}{color(0cm)=(white);
  color(1cm)=(orange!75!red)}
}



\begin{document}

\title{Bayesova optimizacija}

\author{Danijel Pavlek}

\voditelj{prof. dr. sc. Domagoj Jakobović}

\maketitle

\tableofcontents


\chapter{Uvod}
Bayesova optimizacija je primjer algoritma skupe minimizacije koji se koristi kada je funkcija cilja vremenski skupa za evaluaciju. Kao i ostali Bayesovi modeli, i ovaj je temeljen na Bayesovom pravilu koje, uz izglednost, također uzima u obzir i apriorno znanje o problemu. Nadalje, Bayesova optimizacija ne zahtijeva postojanje niti poznavanje derivacija funkcije cilja, što je čini pogodnom za slučajeve gdje se gradijentni postupci, poput najbržeg spusta i Newton-Raphsonovog postupka, ne mogu primijeniti upravo radi tog zahtjeva.



\section{Primjer u stvarnom životu: naftne bušotine}
Uzmimo za primjer bušenje rupa za pronalazak nafte. Tvrtke koje se bave time odrede mjesto na kojem će iskopati rupu i, ako ima nafte, onda kreće njezino izvlačenje. Najčešće se radi o više naftnih bušotina. Valja napomenuti da je cijena bušenja vrlo visoka. Cilj je, dakle, odabrati mjesta iskopa na takav način da profit bude maksimalan. Funkcija kazne kojom se može izraziti koliko dobre odluke su se donijele može se definirati kao razlika između vrijednosti dobivene nafte i novca uloženog u njeno izvlačenje. Valja primijetiti da postoje dvije operacije: odabir mjesta i bušenje. Skuplja operacija je, dakako, bušenje.\\Recimo da ovaj problem rješavamo iscrpnim pretraživanjem (tzv. \textit{grid search}, odnosno pretraživanje površine točku po točku i bušenje svake). Broj bušenja bi kvadratno ovisio o količini podjela horizontalne, odnosno vertikalne osi. Dakle, gubitak bi statistički bio vrlo velik.\\Evolucijski algoritmi također su neisplativi zato što se njihov rad temelji na populaciji, dakle, trebali bismo za svaku jedinku izračunati vrijednost dobrote. Također postoji mogućnost da se zaglavi u lokalnom ekstremu.


\chapter{Algoritam Bayesove optimizacije}
Algoritam koristi dodatnu funkciju u koju možemo ugraditi apriorno znanje (u nastavku funkcija zarade $u$, u većini literature funkcija akvizicije) i na temelju nje izračunavamo argument \textbf{x} za kojeg ćemo evaluirati funkciju cilja. Odabire se argument \textbf{x} koji maksimizira funkciju zarade.\\Druga zanimljiva funkcija je izglednost. Ona se računa temeljem označenog skupa podataka.\\Ovaj algoritam može se shvatiti kao stohastički proces, specifičnije, Gaussov proces. Tada bi funkcija izglednosti predstavljala očekivanje vrijednosti funkcije cilja uz određenu varijancu. Domena bi sadržavala elemente kojima je taj proces indeksiran. Primjerice, u slučaju naftnih bušotina ti elementi bi bili dvodimenzionalni vektori ($x$ i $y$ koordinata rupe). Nakon što se evaluira funkcija cilja (tj. izbuši rupa), očekivanje u toj točki postaje jednako dobivenoj vrijednosti funkcije, a varijanca u toj točki 0. Algoritam se ponavlja sve dok uvjet zaustavljanja nije postignut, što je vrlo često unaprijed definiran broj iteracija.

\section{Gaussovi procesi}{
Bayesova optimizacija koristi ideje modela regresije sa Gaussovim procesom. Kod klasične (diskriminativne) regresije predikcija je čvrsta. Drugim riječima, glavne komponente takvih modela su težine \textbf{w} - nepoznate konstante koje tražimo optimizacijskim algoritmima. Uz to podrazumijevamo da podaci dolaze iz Gaussove distribucije.\\Kod regresije Gaussovim procesom funkciju po kojoj se ravnaju uzorci gledamo kao Gaussov proces, tj. konceptualno ne tražimo jedan model kao u slučaju čvrste regresije, već skup modela.

\begin{figure}[!htb]
\minipage{0.47\textwidth}
  \includegraphics[width=\linewidth]{regression_problem_1}
  \caption{čvrsta regresija}\label{fig:a}
\endminipage
\minipage{0.5\textwidth}
  \includegraphics[width=\linewidth]{sample_bayesian_regression}
  \caption{regresija pomoću Gaussovog procesa}\label{fig:b}
\endminipage
\end{figure}

\subsection{Osnovno}
Stohastički proces je skup slučajnih varijabli indeksiranih po nekoj veličini (npr. vremenu, prostoru i sl.). Gaussov proces podrazumijeva Gaussove distribucije za svaku slučajnu varijablu procesa:$$\mathcal{GP} = \textbf{X} = \{X_t\}_1^{K}|\forall t \in T, X_t\backsim \mathcal{G}(\mu_t, \sigma_t^2)$$$T$ je skup vrijednosti indeksirajuće veličine. Vektor $\textbf{X}$ ima multivarijantnu Gaussovu razdiobu.
U općenitom slučaju ($n$ slučajnih varijabli) vrijedi:$$
\textbf{X}\backsim
\mathcal{N}(
\begin{bmatrix}
\mu_1\\
...\\
\mu_n
\end{bmatrix},
\begin{bmatrix}
\sigma_1^2 & ... & \sigma_{1n}\\
... & ... & ...\\
\sigma_{n1} & ... & \sigma_n^2
\end{bmatrix}
)
$$ili, kraće zapisano: $\textbf{X} \backsim \mathcal{N}(\boldsymbol{\mu}, \textbf{S})$. Kod regresije Gaussovim procesom parametre modela tretiramo kao vrijednosti iz Gaussove razdiobe. Dakle, sama funkcija koja predstavlja model je stohastička komponenta. Pošto funkcije oblika $f:\mathbb{R}^n\rightarrow\mathbb{R}$ možemo zamisliti kao vektor sa beskonačno dimenzija, multivarijantna Gaussova razdioba će biti korisna za opis takvih funkcija. Za Gaussov proces vrijedi:$$
\begin{bmatrix}
f(\textbf{x}^{(1)}) \\
...\\
f(\textbf{x}^{(N)})
\end{bmatrix} \backsim
\mathcal{GP}(
\begin{bmatrix}
\mu_1(t)\\
...\\
\mu_N(t)
\end{bmatrix},
\begin{bmatrix}
\phi(\textbf{x}^{(1)}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(1)}, \textbf{x}^{(N)})\\
... & ... & ...\\
\phi(\textbf{x}^{(N)}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(N)}, \textbf{x}^{(N)})
\end{bmatrix}
)
$$
što se može zapisati kraće:
$\textbf{f} \backsim \mathcal{GP}(\boldsymbol{m}, K)$.

\pagebreak
\subsection{Parametri Gaussovog procesa}
U funkciju očekivanja ugrađuje se apriorno znanje o problemu koji se rješava. Najčešće je to neka konstanta, u većini slučajeva $0$. Možemo kao komponente vektora očekivanja uzeti funkcije vremena $t$. Na donjim slikama prikazani su primjeri Gaussovih procesa sa različitim funkcijama očekivanja $\mu(t)$. Korišteni skup podataka čini niz od $50$ jednoliko raspoređenih brojeva u rasponu $[-5, 5]$.\\\\

\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{f_4sin_2t}
  \caption{$\mu(t)=4sin(2t)$}\label{fig:f_4sin_2t}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{f_05_t_squared}
  \caption{$\mu(t)=\frac{1}{2t^2}$}\label{fig:f_05_t_squared}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{f_exp_minus_01_t}
  \caption{$\mu(t)=0.1\exp(-t)$}\label{fig:f_exp_minus_01_t}
\endminipage
\end{figure}

\begin{figure}[!htb]
\hspace{2cm}
\minipage{0.7\textwidth}
  \includegraphics[width=\linewidth]{game}
  \caption{Primjena Gaussovog procesa u generiranju 1D staze za igru}\label{fig:f_4sin_2t}
\endminipage
\end{figure}

\pagebreak

\setlength{\parindent}{0cm}
Kovarijacijska matrica $K$ govori nam koje varijable su međusobno zavisne i u kolikoj mjeri. Članovi kovarijacijske matrice računaju se koristeći neku jezgrenu funkciju $\phi(\textbf{x}_i, \textbf{x}_j)$ koja je simetrična i pozitivno semidefinitna. Također, mora biti obrnuto proporcionalna euklidskoj udaljenosti, $||\textbf{x}_i - \textbf{x}_j||$ zato što želimo uzeti pretpostavku da točke koje su blizu jedna drugoj imaju slične funkcijske vrijednosti. Primjeri takvih funkcija su:$$K(\textbf{x}_i, \textbf{x}_j)=\beta{}e^{-\gamma||\textbf{x}_i - \textbf{x}_j||^2}\qquad(1)$$ $$K(\textbf{x}_i, \textbf{x}_j)=\frac{1}{1 + (\mathcal{E}||\textbf{x}_i - \textbf{x}_j||)^2}\qquad(2)$$\\Na donjim dijagramima prikazan je Gaussov proces sa očekivanjem $0$ i inverznom kvadratnom jezgrom (2) sa različitim vrijednostima hiperparametrima $\mathcal{E}$.

\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{gp_04}
  \caption{$\mathcal{E}=0.4$}\label{fig:point_four}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{gp_10}
  \caption{$\mathcal{E}=1.0$}\label{fig:one}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{gp_30}
  \caption{$\mathcal{E}=3.0$}\label{fig:three}
\endminipage
\end{figure}
Ukupni broj parametara Gaussovog procesa jednak je broju elemenata vektora očekivanja i kovarijacijske matrice. Ako uzmemo u obzir da je ta matrica simetrična, ukupni broj parametara je $N + \frac{N(N+1)}{2}$. Modeli bazirani na Gaussovom procesu su neparametarski jer broj parametara ovisi o broju primjera.
\pagebreak

\subsection{Modeliranje podataka Gaussovim procesom}{
Zadan je skup označenih podataka za učenje $\mathcal{D} = \{(\textbf{x}^i, y^i)\}_{i=1}^{N}$ i za testiranje $\mathcal{D'} = \{(\textbf{x}^i, y^i)\}_{i=1}^{N'}$. Izlaz $y^i$ može se prikazati kao vrijednost funkcije koja najbolje aproksimira podatke uz dodatak šuma: $$y_i = y(\mathbf{x}_i) = f(\mathbf{x}_i) + \mathcal{E}_i$$Šum dolazi iz Gaussove distribucije: $\mathcal{E} \backsim \mathcal{N}(0, \sigma^2_\epsilon)$. Podaci bez šuma iz skupa za učenje i skupa za testiranje modeliraju se zajedničkim Gaussovim procesom:$$
\begin{bmatrix}
f_1\\
...\\
f_N\\
f_1'\\
...\\
f_N'
\end{bmatrix}\backsim
\mathcal{GP}(
\begin{bmatrix}
\mu_1\\
...\\
\mu_N\\
\mu_1'\\
...\\
\mu_N'
\end{bmatrix},
\begin{bmatrix}

\phi(\textbf{x}^{(1)}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(1)}, \textbf{x}^{(N)}) & \phi(\textbf{x}^{(1)}, \textbf{x}^{(1')}) & ... & \phi(\textbf{x}^{(1)}, \textbf{x}^{(N')})\\

... & ... & ... & ... & ... & ...\\

\phi(\textbf{x}^{(N)}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(N)}, \textbf{x}^{(N)}) & \phi(\textbf{x}^{(N)}, \textbf{x}^{(1')}) & ... & \phi(\textbf{x}^{(N)}, \textbf{x}^{(N')})\\
\phi(\textbf{x}^{(1')}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(1')}, \textbf{x}^{(N)}) & \phi(\textbf{x}^{(1')}, \textbf{x}^{(1')}) & ... & \phi(\textbf{x}^{(1')}, \textbf{x}^{(N')})\\

... & ... & ... & ... & ... & ...\\

\phi(\textbf{x}^{(N')}, \textbf{x}^{(1)}) & ... & \phi(\textbf{x}^{(N')}, \textbf{x}^{(N)}) & \phi(\textbf{x}^{(N')}, \textbf{x}^{(1')}) & ... & \phi(\textbf{x}^{(N')}, \textbf{x}^{(N')})
\end{bmatrix}
)
$$Izraz se može zapisati kraće:$$
\begin{bmatrix}
\textbf{f}\\
\textbf{f}\,{}'
\end{bmatrix} \backsim
\mathcal{GP}(
\begin{bmatrix}
\boldsymbol{\mu}\\
\boldsymbol{\mu}\,{}'
\end{bmatrix},
\begin{bmatrix}
K & K\,{}'\\
K\,{}'{}^T & K\,{}''
\end{bmatrix})$$odnosno: $\mathcal{F} \backsim \mathcal{GP}(\textbf{m}, \textbf{K})$. Korisno je primijetiti kako matrica $K$ ima dimenzije $(N, N)$ i matrica $K\,{}''$ dimenzije $(N\,{}', N\,{}')$. Matrica u gornjem desnom kutu ima dimenzije $(N, N\,{}')$, a pošto je funkcija kovarijacije (jezgrena funkcija) simetrična, matrica u donjem lijevom kutu je transponirana matrica $K\,{}'$ sa dimenzijama $(N\,{}', N)$. Uzmemo li u obzir nezavisno distribuiran šum sa očekivanjem $0$ i varijancom $\sigma^2_{\mathcal{E}}$, jednadžba će biti izmijenjena jedino po pitanju članova kovarijacijske matrice na glavnoj dijagonali (šum je nezavisna varijabla pa se interakcijski članovi matrice ne mijenjaju): $\mathcal{F} \backsim \mathcal{GP}(\textbf{m}, \textbf{K} + \sigma^2_{\mathcal{E}}I)$.\\Aposteriorna razdioba je opet Gaussov proces:$$p(\textbf{f}\,{}'|\textbf{f}) \backsim \mathcal{GP}(\textbf{m}_a, \textbf{K}_a)$$ uz:$$\textbf{m}_a = \boldsymbol{\mu}\,{}' + K\,{}'{}^T(K + \sigma^2_{\mathcal{E}}I)^{-1}(\textbf{f}-\boldsymbol{\mu})$$
$$\textbf{K}_a = K\,{}'' + \sigma^2_{\mathcal{E}}I - K\,{}'{}^T(K + \sigma^2_{\mathcal{E}}I)^{-1}K\,{}'$$Broj elemenata vektora očekivanja aposteriori razdiobe $\textbf{m}_a$ je $N\,{}'$, a dimenzije njezine kovarijacijske matrice $\textbf{K}_a$ su $(N\,{}', N\,{}')$.
}

\subsection{Pojašnjenje vizualizacije}{
Recimo da su vektori $\textbf{f}$ i $\textbf{f}\,{}'$ jednodimenzionalni. Tada je funkcija gustoće zajedničke vjerojatnosti $p(\textbf{f}, \textbf{f}\,{}')$ dvodimenzionalna i sadrži sve informacije potrebne za izračun očekivanja, kovarijacije i intervala pouzdanosti za zadanu funkcijsku vrijednost u nekoj točki. Naime, aposteriorna razdioba nastaje fiksiranjem, tj. rezanjem zajedničke gustoće vjerojatnosti - to je profil prerezane 3D funkcije.\\Recimo da su zadana dva primjera, a samo je prvi označen: $(\textbf{x}^{(1)}, y^{(1)}) = ([2], 5)$ i $(\textbf{x}^{(1\,{}')}, y^{(1\,{}')}) = ([3.8], y)$. Za vektor očekivanja možemo uzeti $\textbf{m} = [5, 0]^T$ (druga vrijednost u vektoru uzima se na temelju apriornih pretpostavki, pošto ih nemamo, možemo uzeti $0$). Recimo da smo odabrali Gaussovu (eksponencijalnu) jezgru, $\phi(\textbf{x}^{(i)}, \textbf{x}^{(j)}) = 2\exp(-0.1||\textbf{x}^{(i)} - \textbf{x}^{(j)}||^2)$. Slika ispod prikazuje funkciju gustoće $p(f, f^{\,{}'})$ uključujući i prerez odozgo (tj. fiksiranje vrijednosti $f$, npr. u vrijednosti $f=6$):
\begin{figure}[!htb]
\minipage{0.48\textwidth}
  \includegraphics[width=\linewidth]{example_process_3d}
  \caption{$p(f, f^{\,{}'})$, 3D prikaz}\label{fig:point_four}
\endminipage\hfill
\minipage{0.48\textwidth}
  \includegraphics[width=\linewidth]{example_process_2d}
  \caption{$p(f, f^{\,{}'})$, 2D prikaz}\label{fig:one}
\endminipage
\end{figure}


Iz aposteriorne distribucije možemo izračunati interval pouzdanosti. Kad bismo postupak proveli za sve primjere $\textbf{x}^{\,{}'}$ iz domene, dobili bismo krivulju očekivanja i površinu pouzdanosti. Površina pouzdanosti je niz intervala pouzdanosti iscrtanih jedan do drugog.

}

\pagebreak
\subsection{Primjer izračuna}{
\textit{Zadana su četiri primjera iz ulaznog prostora $\mathbb{R}^2$: $$(\mathbf{x}^{(1)}, y^{(1)}) = ([0.53\:\:\:\text{-}1.57]^T,\;\text{-}1.0)$$ $$(\mathbf{x}^{(2)}, y^{(2)}) = ([0.25\:\:\:0.32]^T,\;0.8)$$ $$(\mathbf{x}^{(3)}, y^{(3)}) = ([2.61\:\:\:1.07]^T,\;4.0)$$ $$(\mathbf{x}^{(4)}, y^{(4)}) = ([1.72\:\:\:1.55]^T,\;3.5)$$Koristi se eksponencijalna (Gaussova) jezgra, $\phi(\textbf{x}^{(i)}, \textbf{x}^{(j)}) = \exp(-0.05||\textbf{x}^{(i)} - \textbf{x}^{(j)}||^2)$, a kao početni vektor očekivanja uzmite nul-vektor. Treba izračunati početne parametre Gaussovog procesa.}\\\\Vektor očekivanja je: $\boldsymbol{\mu} = [0\:\:\:0\:\:\:0\:\:\:0]^T$. Slijedi izračun kovarijacijske matrice uz zadanu kovarijacijsku funkciju. Npr. element na poziciji $(1, 2)$ je:$$\phi(\textbf{x}^{(1)}, \textbf{x}^{(2)}) = \exp\left[-0.05(\sqrt{(0.25 - 0.32)^2 + (0.32 - (\text{-}1.57))^2})^{\,{}2}\right] = 0.84 = \phi(\textbf{x}^{(2)}, \textbf{x}^{(1)})$$Nakon preostalih računa kovarijacijska matrica izgleda ovako:$$
\begin{bmatrix}
1 & 0.959 & 0.568 & 0.573\\
0.959 & 1 & 0.736 & 0.832\\
0.568 & 0.736 & 1 & 0.950\\
0.573 & 0.832 & 0.950 & 1\\
\end{bmatrix}
$$\textit{Izračunajte očekivanje i $95\%-tni$ interval za vrijednosti funkcije koja najbolje aproksimira zadane podatke, i to u točki $$(\mathbf{x}^{(1\;{}')}) = [2\:\:\:1]^T$$}\\Prvo trebamo ažurirati parametre Gaussovog procesa. Uveden je novi primjer, stoga će se vektor očekivanja povećati za 1, a broj parametara kovarijacije bude veći za 5. Novi vektor očekivanja je $\boldsymbol{\mu} = [0\:\:\:0\:\:\:0\:\:\:0\:\:\:0]^T$ (tu možemo postupiti drugačije, npr. izračunati novu komponentu očekivanja procjenom maksimalne izglednosti). Kovarijacijska matrica će nakon ažuriranja izgledati ovako:$$\begin{bmatrix}
1 & 0.959 & 0.568 & 0.573 & {\color{red} 0.883}\\
0.959 & 1 & 0.736 & 0.832 & {\color{red} 0.950}\\
0.568 & 0.736 & 1 & 0.950 & {\color{red} 0.878}\\
0.573 & 0.832 & 0.950 & 1 & {\color{red} 0.960}\\
{\color{blue} 0.883} & {\color{blue} 0.950} & {\color{blue} 0.878} & {\color{blue} 0.960} & {\color{green} 1}\\
\end{bmatrix}$$Crvenom bojom označena je matrica $\textbf{K\,{}'}$, plavom $\textbf{K\,{}'}^T$, zelenom $\textbf{K\,{}''}$, a crnom $\textbf{K}$. Koristimo formule za vektor očekivanja $\textbf{m}_a$ i kovarijacijsku matricu $\textbf{K}_a$ za aposteriornu razdiobu na kraju odlomka 2.1.3 (pošto je u pitanju samo jedan primjer, rezultati će biti jednodimenzionalni vektor, odnosno matrica). Dobiva se rezultat $\textbf{m}_a=2.78$, $\textbf{K}_a=1.95$ uz $95\%$-tni interval pouzdanosti $(0.05,\;5.51)$.
\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{gaussian_1d_posterior}
  \caption{Aposteriorna distribucija vrijednosti funkcije u točki $\textbf{x}^{(1{}')}$}\label{fig:point_four}
\endminipage
\end{figure}
\\\\\textit{Za sve sljedeće opažene primjere provodili bismo identičan postupak, a vektor očekivanja Gaussovog procesa i kovarijacijska matrica bi se povećavali ukoliko želimo koristiti informacije iz prethodnih koraka. Konkretno, u $i$-tom koraku ovog postupka, uz opažanje jednog primjera po iteraciji i početnih četiri primjera, vektor očekivanja bi imao $i+4$ dimenzije, a kovarijacijska matrica imala bi dimenzije $(i+4, i+4)$. Ako želimo samo ispitati razdiobu vrijednosti u nekoj točki, nije potrebno dodavat točke u Gaussov proces.}
}\clearpage

\section{Algoritam Bayesove minimizacije}
\begin{algorithm}
\caption{Bayes minimization algorithm}
\begin{algorithmic}
\STATE{$\mathcal{D} = \{(\textbf{x}^{(i)}, y^{(i)})\}^N_{i=1}$}
\STATE{\textbf{def} $\;\;u(\textbf{x})$: $m(\textbf{x}) - \kappa{}K(\textbf{x}, \sigma^2_{\mathcal{E}})$}
\WHILE{condition not met}
\STATE{${\textbf{x}}^{(t)} = argmin_{\textbf{x}\in\mathcal{D}}[u(\textbf{x})]$}
\STATE{$y^{(t)} = f(\textbf{x}^{(t)}) + \mathcal{E}^{(t)}$}
\STATE{$\mathcal{D} = \mathcal{D} \cup (\textbf{x}^{(t)}, y^{(t)})$}
\STATE{update Gaussian process}
\ENDWHILE
\end{algorithmic}
\end{algorithm}

Funkcija akvizicije \textit{u}($\textbf{x}^{(i)}$) predstavlja kompromis između očekivanja i standardne devijacije funkcijskih vrijednosti u nekoj točki $\textbf{x}^{(i)}$. Naime, interesantna područja su ona sa malom vrijednošću očekivanja i velikom varijancom. U točki gdje je očekivana vrijednost funkcije manja isplati se istraživati zato što tražimo minimum funkcije $f$. Velika varijanca bi značila slabo istraženo područje, a tamo je i logično pretraživati. Hiperparametar $\kappa$ određuje koliko je kod istraživanja bitna varijanca, a množi se sa $\text{-}1$ jer minimiziramo funkciju, a varijanca je uvijek nenegativni broj. Kod traženja minimuma funkcije $u(\textbf{x}^{(i)})$ možemo koristiti neki standardni postupak minimizacije funkcije $f:\mathbb{R}^n\rightarrow{}\mathbb{R}$.\\Algoritam počinje sa fiksnim skupom točaka, tj. na početku se inicijalizira Gaussov proces. Jedna iteracija algoritma započinje pronalaskom točke $\textbf{x}_t$ u kojoj je najbolji kompromis između očekivanja i standardne devijacije. Algoritam minimizacije funkcije $u$ za svaku točku bi računao aposteriornu razdiobu kako je navedeno u primjeru gore. U toj točki evaluiramo funkciju $f$ čiji se globalni minimum traži. Gaussov proces dobiva nove parametre: dimenzija vektora očekivanja se uveća za jedan, kao i broj dimenzija kovarijacijske matrice. Taj postupak možemo opisati dodavanjem nove točke $(\textbf{x}^{(t)}, y^{(t)})$ u trenutni skup podataka. Tim postupkom vidi se da je model neparametarski.\\Postupak ponavljamo dok nije zadovoljen kriterij zaustavljanja, što može biti unaprijed definiran broj iteracija ili pronađeno dovoljno dobro rješenje $\textbf{x}\,'$ za koje vrijedi $f(\textbf{x}\,') < \mathcal{E}$.\\\\Slijedi primjer Bayesove optimizacije uz prikaz Gaussovog procesa i funkcije akvizicije. Korištena je akvizicijska funkcija iz pseudokoda uz parametar $\kappa=5$ i algoritam LM-BFGS za njezinu minimizaciju. Funkcija cilja je $$f(x)=-50\sum_{i=1}^{N}e^{\frac{-(x-\mu_{i})^2}{\sigma_{i}^2}}$$uz $\mu_i \in \{-7.5, -2, 4, 8, 10, 15\}$, $\sigma_i \in \{5, 40, 40, 1, 100, 1\}$ i $N=6$.

\pagebreak
\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{problem_1/iter_1}
  \caption{Stanje algoritma na početku - poznate su dvije točke}\label{fig:point_four}
\endminipage
\end{figure}
\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{problem_1/iter_2}
  \caption{Nakon prve iteracije smanjila se površina pouzdanosti}\label{fig:point_four}
\endminipage
\end{figure}
\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{problem_1/iter_3}
  \caption{Stanje nakon druge iteracije}\label{fig:point_four}
\endminipage
\end{figure}
\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{problem_1/iter_4}
  \caption{U trećoj iteraciji površina pouzdanosti je veća radi povećanja parametra $\textbf{m}_a$}\label{fig:point_four}
\endminipage
\end{figure}
\pagebreak
$$$$
$$$$
$$$$
$$$$

\section{Hiperparametri modela}{
Hiperparametri algoritma Bayesove minimizacije bili bi hiperparametri Gaussovog procesa i funkcija akvizicije $u(\textbf{x})$.

\subsection{Vektor očekivanja Gaussovog procesa}{
Za vektor očekivanja najčešće se uzima nul-vektor, međutim, može biti i neka druga konstanta ili funkcija, posebice ako korisnik sluti kakva bi mogla biti ta funkcija.
}

\subsection{Kovarijacijska funkcija Gaussovog procesa}{
Hiperparametri koji proizlaze iz kovarijacijske matrice Gaussovog procesa su kovarijacijska funkcija i njezini parametri. Kao kovarijacijska funkcija najčešće se bira Gaussova jezgra, međutim, može biti bilo koja simetrična pozitivna funkcija, dakle, rezultat će biti simetrična pozitivno definitna matrica što također predstavlja pogodnost u računanju inverza - simetrične pozitivno definitne matrice imaju sve strogo pozitivne svojstvene vrijednosti, dakle, moguće je iskoristiti faktorizaciju Choleskog umjesto standardne LUP dekompozicije: $\textbf{K} = \textbf{L}\textbf{L}^T$, gdje je $\textbf{L}$ donja trokutasta matrica.
}

\subsection{Funkcija akvizicije}{
Funkcija akvizicije navedena u pseudokodu algoritma samo je jedna od više mogućih (UCB - tj. upper confidence bound). Nadalje, može se maksimizirati očekivano poboljšanje u odnosu na najbolju točku, nesigurnost u okolini najbolje točke i sl.
}

\subsection{Algoritam optimizacije funkcije akvizicije}{
Funkcija akvizicije može biti, a često i jest multimodalna, zato je potrebno odabrati odgovarajući algoritam optimizacije imajući na umu njegovu računalnu složenost jer je već složenost samog algoritma Bayesove optimizacije vrlo velika.
}

}


\chapter{Primjena na minimizaciju funkcije}
\section{Jednostavan primjer}
Uzmimo za primjer minimizaciju višemodalne funkcije dvije varijable:$$f(x_1, x_2)=-20\sum_{i=1}^{N}A_i{\;}e^{\frac{-(x_1-\mu_{i,1})^2-(x_2-\mu_{i,2})^2}{\sigma_{i}^2}}$$Uz $\mu_{i, 1} \in \{\text{-}3, 2, \text{-}2, 1, 4\}$, $\mu_{i, 2} \in \{\text{-}2, \text{-}2, 0, 0.5, 4\}$, $\sigma_i \in \{4, 2, 50, 1, 20\}$ i $A_i \in \{9, 13, 11, 2, 12\}$ ta funkcija izgleda ovako:\\
\begin{center}
\includegraphics[scale=0.4]{problem_2/func}
\end{center}
\subsection{Gradijentni spust}
Gradijentni spust, kao što je poznato, može zaglaviti u lokalnom minimumu. Nadalje, javlja se problem prisutan kod izduženih koordinatnih osi. Dakle, izbor početne točke već u početku može spriječiti ovaj algoritam da pronađe zadovoljavajuće rješenje, neovisno o tome računa li se optimalni iznos gradijenta ili ne.

\subsection{Bayesova optimizacija}
Algoritam Bayesove optimizacije nije efikasan kad se primijeni samostalno na neki problem, već se mora kombinirati sa postojećim algoritmima koji pretražuju prostor lokalno počevši od točke koja se pronađe Bayesovom optimizacijom.

\subsection{Kombinacija gradijentnog spusta i Bayesove optimizacije}
U ovom primjeru iskorišten je algoritam koji kombinira Bayesovu optimizaciju i gradijentni spust na način da se Bayesovom optimizacijom pronađe zanimljiva točka i zatim se provede gradijentni spust sa određenim brojem iteracija. Pošto je funkcija optimalnog koraka potencijalno višemodalna, on nije optimiran zlatnim rezom nego linearnim pretraživanjem.\\
Najbolje rješenje je točka $(2, -2)$ gdje se postiže funkcijska vrijednost $-440.327$. Algoritam je u $16$ koraka Bayesove optimizacije i u svakom $100$ gradijentnog spusta pronašao točku $(-0.715, 3.073)$ iz koje je algoritam gradijentnim spustom došao u točku $(1.742, -1.554)$ u kojoj je funkcijska vrijednost $-426.822$.

\begin{figure}[!htb]
\minipage{1\textwidth}
  \includegraphics[width=\linewidth]{scheme}
  \caption{Shema eksperimenta}\label{fig:point_four}
\endminipage
\end{figure}

\chapter{Primjena u strojnom učenju}
\section{Optimizacija hiperparametara neuronske mreže}
\subsection{Primjer 1}
Zadana je neuronska mreža sa ulaznim, dva skrivena i jednim izlaznim slojem. Model treba naučiti klasifikaciju dvodimenzionalnih ulaznih podataka uz optimiranje broja neurona u pojedinom sloju i početnu stopu učenja.\\Za ovaj problem iskorištena je implementacija višeslojnog perceptrona iz programskog paketa \texttt{sklearn} (Python 3.7). Skup ulaznih podataka je programski generiran:
\begin{center}
\includegraphics[scale=0.7]{problem_5/easy_dataset}
\end{center}
U algoritmu Bayesove optimizacije jedna evaluacija funkcije bila bi stvaranje neuronske mreže sa zadanim hiperparametrima i učenje sa $100$ iteracija algoritmom \textit{adam}. Skup podataka je podijeljen na dva dijela: skup za učenje i validaciju. Domena pretraživanja za varijable koje predstavljaju broj slojeva je $\left[2, 20\right]$ za oba broja neurona po sloju te $\left[0, 1\right]$ za stopu učenja. Postoji implementacijski detalj: broj neurona po svakom sloju je prirodni broj, tako da se u Bayesovoj optimizaciji mora osigurati zaokruživanje na bliži cijeli broj. To je najbolje učiniti u trenutku kad se traži minimum akvizicijske funkcije - pokazalo se da je dovoljno zaokružiti pronađeni minimum na prvu manju cjelobrojnu vrijednost.\\Algoritam je smanjio klasifikacijski gubitak na $0$ nakon $9$ evaluacija funkcije cilja. Moguće poboljšanje bi bilo optimiranje regularizacijskog faktora i broja iteracija algoritma učenja modela gdje bi se velik broj iteracija mogao značajno kazniti zbog trošenja procesorskog vremena. Naposlijetku, moguće je kažnjavati i preveliki broj neurona, ali tek ako je klasifikacijski gubitak mali.\\Najbolje rješenje je neuronska mreža sa $14$ neurona u prvom i $11$ u drugom skrivenom sloju sa početnom stopom učenja $0.1373$. U $25.$ iteraciji algoritam je pronašao jednostavniju arhitekturu, sa $17$ neurona u prvom i $3$ u drugom skrivenom sloju uz stopu učenja $0.4684$. Interesantno je kako je algoritam u par iteracija kasnije pronašao rješenje identično prethodnom, samo uz stopu učenja $0.0566$. Također, pronađena je arhitektura sa $17$ neurona u prvom i $2$ u drugom sloju, a uz stopu učenja $0.3965$ klasifikacijski gubitak je bio skoro dvostruko veći: $0.452$. Sadržaj svih iteracija može se pronaći na \href{https://gitlab.com/dp48687/bayesian-optimization}{\color{blue} \textit{https://gitlab.com/dp48687/bayesian-optimization}}.
\subsection{Primjer 2}
U ovom primjeru zadatak je također naučiti neuronsku mrežu, međutim, na složenijem (nelinearnijem) skupu podataka i s više mogućih hiperparametara. Optimira se, uz broj neurona po svakom sloju i inicijalnu stopu učenja, i vrsta algoritma učenja (adam, stohastički gradijentni spust i LBFGS) na sljedećem skupu podataka (podaci su programski generirani, kao i u prethodnom primjeru):
\begin{center}
\includegraphics[scale=0.7]{problem_6/hard_dataset}
\end{center}
Pokazalo se da je ovo previše težak problem, neuronska mreža je postigla rezultate malo bolje od pogađanja izlaza, a stroj s potpornim vektorima je ostvario klasifikacijski gubitak od $0.111$.
\section{Klasifikacija Gaussovim procesom}
\subsection{Primjer 3}
Ovo je mala digresija od stvarne teme. Na drugom skupu podataka Gaussov proces je iskorišten u domeni klasifikacije i dao je impresivne rezultate koji su prikazani na slici ispod. Za primjer, neuronska mreža sa optimizacijom hiperparametara davala je malo bolje rezultate od slučajnog svrstavanja primjera u klase (točnije, klasifikacijski gubitak bio je 0.495), dok je Gaussov proces imao klasifikacijski gubitak $0.007$. Spomenuti primjer nalazi se u modulu \textit{problem\_{}set} u funkciji \textit{problem10}.
\begin{center}
\includegraphics[scale=0.45]{problem_7/gpc}
\end{center}

\chapter{Zaključak}
Bayesova optimizacija može biti koristan algoritam kad treba optimirati funkciju računalno zahtjevnu za evaluaciju - moguće je da će se isplatiti iskoristiti taj algoritam, međutim, treba pomno analizirati problem jer Gaussov proces treba potencijalno dosta točaka kako bi sam algoritam napredovao što može samo po sebi biti računalno zahtjevno. Također, Bayesova optimizacija često može davati loše rezultate kad se upotrebljava samostalno - nekad je bolje nakon računanja parametara aposteriorne distribucije napraviti lokalnu pretragu algoritmima koji su predviđeni za to. Drugim riječima, treba napraviti kompromis između istraživanja i eksploatacije. Ukoliko spomenuti algoritam ne generira zadovoljavajuće rezultate, treba upotrijebiti alternativu poput pretraživanja po rešetci, slučajnog pretraživanja i sl.


\bibliography{literatura}
\bibliographystyle{fer}


\chapter{Sažetak}
U ovom radu pojašnjen je algoritam Bayesove optimizacije i njegove prednosti i nedostaci. Opisani su Gaussovi procesi i rješen je numerički primjer na kojem se može shvatiti kako Gaussov proces generira nove točke. Na kraju je pokrenuto nekoliko programskih primjera kojima se testira rad algoritma Bayesove optimizacije na optimiranju jednostavnih višemodalnih funkcija i parametara neuronske mreže i stroja s potpornim vektorima.


\end{document}