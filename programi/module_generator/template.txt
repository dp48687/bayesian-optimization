// https://yizhang82.dev/python-interop-capi - this is version for python 2.x
//
// info:
// https://docs.python.org/3/extending/extending.html
// https://stackoverflow.com/questions/24226001/importerror-dynamic-module-does-not-define-init-function-initfizzbuzz


#define PY_SSIZE_T_CLEAN
#include<Python.h>
#include<stdio.h>


// How should we call the method below from Python:
// method(((arg1, arg2), (arg3, arg4)), (arg5, arg6))
// identifiers:
// i - integer
// s - string
// l - long int
// k - unisgned long
// L - long long
// K - unsigned long long
// f - float
// d - double
//
// more: https://docs.python.org/3/c-api/arg.html#arg-parsing


$method_definitions


static PyMethodDef method_pool[] = {
$method_infos,
    {NULL, NULL, 0, NULL}
};


static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    "$compiled_file_name",
    "NULL",
    -1,
    method_pool
};


PyMODINIT_FUNC PyInit_$compiled_file_name(void)
{
    return PyModule_Create(&module);
}