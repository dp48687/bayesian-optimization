import os


def method_definition():
    return "static PyObject* $method_name(PyObject* self, PyObject* args) {" +\
            "$parsing_stuff" +\
            "\n\treturn self;\n}"


def method_info(name):
    return "\t{'$method_name', $method_name, METH_VARARGS, 'Method $method_name.'}"\
        .replace("$method_name", name)\
        .replace("'", '"')


def build_parameters(structure):
    built = ""
    var_count = 1
    for char in structure:
        if char == "i":
            built += f"\tint var{var_count};\n"
        elif char == "l":
            built += f"\tlong int var{var_count};\n"
        elif char == "k":
            built += f"\tunsigned long var{var_count};\n"
        elif char == "L":
            built += f"\tlong long var{var_count};\n"
        elif char == "K":
            built += f"\tunsigned long long var{var_count};\n"
        elif char == "f":
            built += f"\tfloat var{var_count};\n"
        elif char == "d":
            built += f"\tdouble var{var_count};\n"
        elif char == "s":
            built += f"\tchar* var{var_count};\n"
        else:
            var_count -= 1
        var_count += 1
    return built, var_count-1


def make_address_list(n_vars):
    return "".join([f"&var{i}, " for i in range(1, n_vars + 1)])[:-2] if n_vars > 0 else ""


def build_method(name, structure):
    var_declaration, n_vars = build_parameters(structure)
    parsing_stuff = (f"\n{var_declaration}\tif (!PyArg_ParseTuple(args, ${structure}$, {make_address_list(n_vars)})) " +\
                    "{\n\t\tprintf(\"Error happened during parameter parsing.\");\n\t\treturn NULL;" +\
                    "\n\t}").replace("$", '"') if n_vars>0 else ""

    return method_definition()\
        .replace("$parsing_stuff", parsing_stuff)\
        .replace("$method_name", name)\
        .replace("return self;", "\n\t//Write your implementation here.\n\t\n\treturn self;")


def build_methods(method_names, method_structures, module_name="generated_module"):
    methods = ""
    infos = ""
    for name, structure in zip(method_names, method_structures):
        methods += build_method(name, structure)
        methods += "\n\n"
        infos += method_info(name)
        infos += ",\n"
    return methods[:-2], infos[:-2]


if __name__ == "__main__":
    SOURCE_FILE_NAME = 'ai_generated.cpp'
    COMPILED_FILE_NAME = "ai_generated_compiled"
    # DO NOT FORGET TO PUT A COMMA AT THE END OF THE ARGUMENTS!
    METHOD_NAMES = ("py_minimize_ANF",)
    # SAME THING HERE.
    METHOD_STRUCTURES = ("()",)
    FOLDER = SOURCE_FILE_NAME[:-2]

    if not os.path.exists(FOLDER):
        os.makedirs(FOLDER)
    else:
        i = 1
        while os.path.exists(FOLDER + f"_{i}"):
            i += 1
        FOLDER += f"_{i}"
        os.makedirs(FOLDER)

    bodies, infos = build_methods(METHOD_NAMES, METHOD_STRUCTURES)
    with open("template.txt", "r") as opened_template:
        template = opened_template.read()
        template = template.replace("$method_definitions", bodies)\
                            .replace("$method_infos", infos)\
                            .replace("$compiled_file_name", COMPILED_FILE_NAME)
        with open(os.path.join(FOLDER, SOURCE_FILE_NAME), "w") as generated:
            generated.write(template)

    with open("compile_template.txt", "r") as template_compile:
        template = template_compile.read()
        template = template.replace("COMPILED_FILE = '...'", f"COMPILED_FILE = '{COMPILED_FILE_NAME}'")\
                            .replace("SOURCE_FILE_NAME = '...'", f"SOURCE_FILE_NAME = '{SOURCE_FILE_NAME}'")
        with open(os.path.join(FOLDER, f"compile_{SOURCE_FILE_NAME}".replace(".c", ".py")), "w") as to_write:
            to_write.write(template)

    with open(os.path.join(FOLDER, f"run_{SOURCE_FILE_NAME}".replace(".c", ".py")), "w") as to_write:
        to_write.write(
            f"import module_generator.{FOLDER}.{COMPILED_FILE_NAME} as imported\n\n\n" +
            f"if __name__ == '__main__':\n\tpass\n"
        )
