from distutils.core import setup, Extension


# We have to execute this module using command:
#
#       python3 compile_template.txt build
#
# and then all .c files get compiled and we can then
# import those compiled files and use them in our python
# scripts.


if __name__ == "__main__":
    module1 = Extension('sample1_compiled', sources=['sample1.c'])
    setup(version='1.0', description='This is a demo package', ext_modules=[module1])
