// https://yizhang82.dev/python-interop-capi - this is version for python 2.x
//
// info:
// https://docs.python.org/3/extending/extending.html
// https://stackoverflow.com/questions/24226001/importerror-dynamic-module-does-not-define-init-function-initfizzbuzz


#define PY_SSIZE_T_CLEAN
#include<Python.h>
#include<stdio.h>


// How should we call the method below from Python:
// method(((arg1, arg2), (arg3, arg4)), (arg5, arg6))
// identifiers:
// i - integer
// s - string
// l - long int
// k - unisgned long
// L - long long
// K - unsigned long long
// f - float
// d - double
//
// more: https://docs.python.org/3/c-api/arg.html#arg-parsing

static PyObject* method(PyObject *self, int *args, int length) {
    printf("Hello: %d %d\n", args[0], args[1]);
    printf("Length: %d\n", length);
    return PyLong_FromLong(*(args + 1));
}


static PyMethodDef SpamMethods[] = {
    {"method",  method, METH_VARARGS, "A probe method which returns 3.14."},
    {NULL, NULL, 0, NULL}
};


static struct PyModuleDef spammodule = {
    PyModuleDef_HEAD_INIT,
    "sample1_compiled",
    "NULL",
    -1,
    SpamMethods
};


PyMODINIT_FUNC PyInit_sample1_compiled(void)
{
    return PyModule_Create(&spammodule);
}