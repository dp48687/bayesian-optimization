#include  "AI.h"

typedef unsigned int uint;

uint size = 8;

//int main()
//{
//	uint anf[] = { 1, 2, 4, 6, 7, 12, 14, 17, 18, 19, 23, 25, 26, 27, 28, 31, 33, 34, 35, 39, 40, 41, 42, 43, 44, 45, 46, 50, 51, 53, 56, 59, 60, 61, 62, 64, 66, 67, 68, 69, 70, 77, 79, 81, 82, 84, 86, 88, 90, 91,93, 95, 96, 100, 104, 106, 110, 112, 116, 117, 118, 120, 128, 130, 134, 139, 142, 144, 146, 148, 151, 153, 156, 157, 158, 159, 161, 163, 164, 166, 175, 178, 181, 182, 183, 184, 189, 190, 192, 194, 196, 198, 199, 201, 203, 205, 206, 207, 210, 216, 217, 220, 221, 223, 224, 225, 229, 230, 232, 238, 243, 244, 245, 246, 247, 250, 252, 254 };
//	uint ai = calculate(anf, size);
//}


// test AI:
// tt i anfPos su int vektori velicine tablice istinitosti (2^nVariables)
// u vektoru tt je zapisana talbica istinitosti koja se optimira
//minimizeANF(&tt[0], nVariables, &anfPos[0]);
//int ai = calculate(&anfPos[0], nVariables);
//cout << ", ai: " << ai;


int minimizeANF(int *tt, int n, unsigned int* anfpos)
{
	int i, j, k, rezultat = 0;

	static int *t = 0, *u = 0, *rez = 0;

	if (t == 0) {
		rez = (int *)malloc((1 << n)*sizeof(int));
		u = (int *)malloc(((1 << n) >> 1)*sizeof(int));
		t = (int *)malloc(((1 << n) >> 1)*sizeof(int));
	}

	for (i = 0; i < (1 << n); ++i)
		rez[i] = tt[i];
	for (i = 0; i < ((1 << n) >> 1); ++i)
		u[i] = t[i] = 0;

	for (i = 0; i < n; ++i)
	{
		for (j = 0; j < ((1 << n) >> 1); ++j)
		{
			t[j] = rez[2 * j];
			u[j] = (rez[2 * j] == rez[2 * j + 1]) ? 0 : 1;
		}
		for (k = 0; k<((1 << n) >> 1); ++k)
		{
			rez[k] = t[k];
			rez[((1 << n) >> 1) + k] = u[k];
		}
	}

	for (i = 0; i < (1 << n); i++)
	{
		if (rez[i] == 1) {
			anfpos[rezultat] = i;
			rezultat++;
		}	
	}

	//	free(rez);
	//	free(u);
	//	free(t);

	return rezultat;
}


uint calculate(uint ones_indexes[], uint n_vars) {
	struct elements *elems = getFiberOver1(ones_indexes, n_vars);

	return siouksAlgorithm(elems, 3 * (2621440000 / 4)); // max 2GB=2621440000
	
}

struct elements* getFiberOver1(uint ones_indexes[], uint n_vars) {
	struct elements *elems;
	long counter;
	uint max;
	uint *tmp;

	elems = (elements*)malloc(sizeof(struct elements));
	if (!elems) {
		return NULL;
	}

	max = (~0u) >> (MON_SIZE - n_vars);
	tmp = (uint*)calloc(max + 1, sizeof(uint));
	if (!tmp) {
		free((void*)elems);
		return NULL;
	}

	counter = 0;
	for (uint i = 0; i <= max; ++i) {
		if (evaluate(ones_indexes, i) == 1) {
			tmp[counter] = i;
			++counter;
		}
	}

	elems->num_elms = counter;
	elems->num_vars = n_vars;
	elems->e = (uint*)adjustMemory(tmp, counter* sizeof(uint));

	return elems;
}

void* adjustMemory(void *ptr, size_t size) {
	void *m;
	if (size == 0) {
		free((void*)ptr);
		return NULL;
	}

	if (ptr == NULL) {
		m = malloc(size);
	}
	else {
		m = realloc(ptr, size);
	}

	return m ? m : ptr;
}

uint evaluate(uint ones_indexes[], long x) {
	uint res = 0;
	int len = 1 << size;

	for (long i = 0; i < len; ++i) {
		res ^= evaluateMonomial(ones_indexes[i], x);
	}

	return res;
}

uint evaluateMonomial(uint m, uint x) {
	return (~(((~m) | x) - (~0u))) >> (MON_SIZE - 1);
}

void sortElements(struct elements *elems) {
	qsort(elems->e, elems->num_elms, sizeof(uint), monomial_compare);
}

long hamming(uint x) {
	long w = 0;
	for (long i = 0; i < (long)MON_SIZE; ++i) {
		if (x & (1u << i)) {
			++w;
		}
	}

	return w;
}

int monomial_compare(const void *x, const void *y) {
	uint a = ((uint*)x)[0];
	uint b = ((uint*)y)[0];

	long wa = hamming(a);
	long wb = hamming(b);

	if (wa != wb) {
		return (int)(wa - wb);
	}

	for (long i = MON_SIZE - 1; i >= 0; --i) {
		uint ba = (a >> i) & 1u;
		uint bb = (b >> i) & 1u;

		if (ba == bb) { continue; }
		if (ba == 1u) { return 1; }
		if (bb == 1u) { return -1; }
	}

	return 0;
}

uint saff_eval(long var, uint a) {
	return (a >> var) & 1u;
}


unsigned long safe_add(unsigned long a, unsigned long b) {
	unsigned long r;
	if (a == ulong_max || b == ulong_max) return ulong_max;
	r = a + b;
	if (r < a) return ulong_max;
	return r;
}

unsigned long safe_sub(unsigned long a, unsigned long b) {
	if (a == ulong_max) return ulong_max;
	if (b > a) return 0;
	return a - b;
}

unsigned long safe_mul(unsigned long a, unsigned long b) {
	if (a == ulong_max || b == ulong_max) return ulong_max;
	if (a == 0ul) return 0ul;
	if (b > ulong_max / a) return ulong_max;
	return a * b;
}

unsigned long safe_div(unsigned long a, unsigned long b) {
	if (a == ulong_max) return ulong_max;
	if (b == ulong_max) return 0;
	if (b == 0ul) return ulong_max;
	return a / b;
}

unsigned long safe_sum(unsigned long n) {
	return safe_div(safe_mul(n, safe_add(n, 1)), 2);
}

long safe_s_matrix_formula(long i) {
	unsigned long tmp, offset;
	tmp = safe_mul(safe_div(i, NB), safe_sum(NB));
	tmp = safe_add(tmp, safe_sum(i % NB));
	offset = safe_div(safe_sub(safe_sum(i), tmp), NB);
	offset = safe_add(i, offset);
	if (offset >= (unsigned long)LONG_MAX) return LONG_MAX;
	return offset;
}

long s_matrix_formula(long i) {
	long tmp, offset;
	tmp = (i / NB) * sum(NB) + sum(i % NB);
	offset = i + ((sum(i) - tmp) / NB);
	return offset;
}

unsigned long sum(unsigned long long n) {
	return (n * (n + 1)) / 2ull;
}

long s_matrix_ineq(long M) {
	long x, x1, x2, y;
	/* find an interval containing the solution */
	for (x1 = 1, x2 = 2;
		safe_s_matrix_formula(x2) < M;
		x1 = x2, x2 = x2 * x2);
	/* dichotomy to find the solution */
	for (; x2 - x1 > 1;) {
		x = x1 + (x2 - x1) / 2;
		y = s_matrix_formula(x);
		if (y > M) { x2 = x; continue; }
		if (y < M) { x1 = x; continue; }
		return x;
	}
	return x1;
}

struct s_matrix *s_matrix_malloc(long n, long mem) {
	long ncoeff, alloc, nbuf;
	unsigned char *coeff, *buf;
	struct s_matrix *M;

	alloc = s_matrix_ineq(mem);
	ncoeff = s_matrix_formula(alloc);

	coeff = (unsigned char*)malloc(ncoeff * sizeof(unsigned char));
	if (!coeff) {
		return NULL;
	}

	nbuf = (alloc + 1) / NB + 1;
	buf = (unsigned char*)malloc(nbuf * sizeof(unsigned char));
	if (!buf) {
		free((void*)coeff);
		return NULL;
	}

	memset(coeff, s_empty, ncoeff * sizeof(unsigned char));
	M = (struct s_matrix*)malloc(sizeof(struct s_matrix));
	if (!M) {
		free((void*)buf);
		free((void*)coeff);
		return NULL;
	}

	M->n = n;
	M->n_coeff = ncoeff;
	M->coefficient = coeff;
	M->alloc = alloc;
	M->buffer = buf;
	M->n_buf = nbuf;

	return M;
}

void freeN(g_newton* N, long i, long d) {
	for (long j = 0; j <= i && j < d; ++j) {
		if (N[j].sum) {
			free(N[j].sum);
		}
	}

	free(N);
}

void freeN(s_newton* N, long i, long d) {
	for (long j = 0; j <= i && j < d; ++j) {
		if (N[j].sum) {
			free(N[j].sum);
		}
	}

	free(N);
}

void freeM(s_matrix *M) {
	free((void*)M->coefficient);
	free((void*)M);
}


uint next_monomial(uint mo, long n_var) {
	long i, w, h;
	const uint mask = MASK(2);

	if (mo == 0u) return 1u;
	h = n_var - 1;
	for (i = h - 1; i >= 0 && ((mo >> i) & mask) != 1u; i--);
	if (i < 0) return (~0u) >> (MON_SIZE - hamming(mo) - 1);
	if ((mo >> h) & 1u) {
		w = hamming(mo >> i);
		return (mo & MASK(i)) | (MASK(w) << (i + 1));
	}
	else {
		return mo ^ (mask << i);
	}
}

void quo_monomial(long *var, uint *r, uint a, long n_var) {
	long i;
	if (a == 0u) {
		var[0] = -1;
		r[0] = a;
	}

	for (i = 0; i < n_var && !((a >> i) & 1u); i++);
	var[0] = i;
	r[0] = a ^ (1u << i);
}

uint s_newton_eval2_buf(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M)
{
	long i;
	uint ret;
	struct s_newton *curr = &N[Nid];
	ret = saff_eval(curr->var, elems->e[ptid]) &
		s_newton_eval_buf(N, curr->right, elems, ptid, M);

	for (i = 0; i < curr->n_sum; i++) {
		ret ^= s_newton_eval_buf(N, curr->sum[i], elems, ptid, M);
	}
	return ret;
}

uint s_newton_eval_buf(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M) {
	uint ret;

	if (Nid == 0) { return 1u; }
	if (N[Nid].id != -1) { /* maybe precomputed */
		if (Nid > ptid) {
			ret = 0u;
		}
		else {
			unsigned char c;
			long offset, shift;
			offset = Nid / NB;
			shift = WIDTH * (Nid % NB);
			c = M->buffer[offset];
			ret = (((uint)c) >> shift) & MASK(WIDTH);
			if (ret != s_undef) return ret;
			ret = s_newton_eval2_buf(N, Nid, elems, ptid, M);
			c = M->buffer[offset]; /* NECESSARY */
			c &= ~(MASK(WIDTH) << shift);
			c |= ret << shift;
			M->buffer[offset] = c;
		}
	}
	else {
		ret = s_newton_eval2_buf(N, Nid, elems, ptid, M);
	}
	return ret;
}

uint s_newton_eval_void(struct s_newton *N, long Nid, struct elements *elems, long ptid) {
	if (Nid == 0) return 1u;
	return s_newton_eval2_void(N, Nid, elems, ptid);
}

uint s_newton_eval2_void(struct s_newton *N, long Nid, struct elements *elems, long ptid) {
	long i;
	uint ret;
	struct s_newton *curr = &N[Nid];
	ret = saff_eval(curr->var, elems->e[ptid]) & s_newton_eval_void(N, curr->right, elems, ptid);

	for (i = 0; i < curr->n_sum; i++) {
		ret ^= s_newton_eval_void(N, curr->sum[i], elems, ptid);
	}

	return ret;
}

uint s_newton_eval2_mem(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo) {
	long i;
	uint ret;
	struct s_newton *curr;

	curr = &N[Nid];
	ret = saff_eval(curr->var, elems->e[ptid]) &
		s_newton_eval_mem(N, curr->right, elems, ptid, M, lo);
	for (i = 0; i < curr->n_sum; i++) {
		ret ^= s_newton_eval_mem(N, curr->sum[i], elems, ptid, M, lo);
	}
	return ret;
}

uint s_newton_eval_mem(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo) {
	uint ret;
	if (Nid == 0) return 1u;
	if (N[Nid].id != -1) { /* maybe precomputed */
		if (Nid > ptid) {
			ret = 0u;
		}
		else {
			unsigned char c;
			long offset, shift;
			/*offset = address(M,ptid,Nid) + (Nid / NB);*/
			offset = lo + (Nid / NB);
			shift = WIDTH * (Nid % NB);
			c = M->coefficient[offset];
			ret = (((uint)c) >> shift) & MASK(WIDTH);
			if (ret != s_undef) return ret;
			ret = s_newton_eval2_mem(N, Nid, elems, ptid, M, lo);
			c = M->coefficient[offset]; /* NECESSARY */
			c &= ~(MASK(WIDTH) << shift);
			c |= ret << shift;
			M->coefficient[offset] = c;
		}
	}
	else {
		ret = s_newton_eval2_mem(N, Nid, elems, ptid, M, lo);
	}
	return ret;
}


uint s_newton_eval(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo) {
	if (Nid >= M->alloc) {
		return s_newton_eval_void(N, Nid, elems, ptid);
	}
	else if (ptid >= M->alloc) {
		memset(M->buffer, s_empty, M->n_buf * sizeof(unsigned char));
		return s_newton_eval_buf(N, Nid, elems, ptid, M);
	}
	else {
		return s_newton_eval_mem(N, Nid, elems, ptid, M, lo);
	}
}

long siouks_alg_next_lo(long lo, long i) {
	long l;
	l = i + 1;
	return lo + l / NB + (l % NB ? 1 : 0);
}

void s_newton_update(struct s_newton *N, long Nid, long j) {
	struct s_newton *curr = &N[Nid];
	curr->sum = (long*)adjustMemory(curr->sum, (curr->n_sum + 1) * sizeof(long));
	curr->sum[curr->n_sum] = j;
	curr->n_sum++;
}

void siouks_alg_xchg(struct s_matrix *M, struct elements *elms, long i, long j) {
	uint t;
	unsigned char c;
	long k, offseti, offsetj, tmp;

	if (i == j) { return; }
	if (i > j) {
		tmp = i;
		i = j;
		j = tmp;
	}

	/* exchange the points */
	t = elms->e[i];
	elms->e[i] = elms->e[j];
	elms->e[j] = t;

	/* lines are not in memory */
	if (i >= M->alloc) {
		return;
	}

	/* one line is in memory and not the other */
	if (i < M->alloc && j >= M->alloc) {
		offseti = s_matrix_formula(i);
		i++;
		for (k = 0; k < (i / NB); k++) {
			M->coefficient[offseti + k] = M->buffer[k];
		}
		if (i % NB) {
			unsigned char lo2, hi1, mask;
			long shift;
			shift = WIDTH * (i % NB);
			mask = MASK(shift);
			hi1 = M->coefficient[offseti + k] >> shift;
			lo2 = M->buffer[k] & mask;
			M->coefficient[offseti + k] = lo2 | (hi1 << shift);
		}
		return;
	}

	/* exchange the lines of the matrix */
	offseti = s_matrix_formula(i);
	offsetj = s_matrix_formula(j);
	i++;
	for (k = 0; k < (i / NB); k++) {
		c = M->coefficient[offseti + k];
		M->coefficient[offseti + k] = M->coefficient[offsetj + k];
		M->coefficient[offsetj + k] = c;
	}
	if (i % NB) {
		unsigned char lo1, lo2, hi1, hi2, mask;
		long shift;
		shift = WIDTH * (i % NB);
		mask = MASK(shift);
		lo1 = M->coefficient[offseti + k] & mask;
		hi1 = M->coefficient[offseti + k] >> shift;
		lo2 = M->coefficient[offsetj + k] & mask;
		hi2 = M->coefficient[offsetj + k] >> shift;
		M->coefficient[offseti + k] = lo2 | (hi1 << shift);
		M->coefficient[offsetj + k] = lo1 | (hi2 << shift);
	}
}

uint siouksAlgorithm(struct elements *elems, unsigned long memory) {
	long d = elems->num_elms, u, v;
	long ai = -1, n_var, lo;
	long j;

	uint current = 0u, quo;

	struct s_matrix *M;
	struct s_newton *N;

	n_var = elems->num_vars;
	if (n_var == 0) {
		return 0;
	}

	sortElements(elems);

	long *s_newton_bij = (long*)calloc(1 << n_var, sizeof(long));
	if (!s_newton_bij) {
		return -1;
	}
	s_newton_bij[0] = 0;

	N = (struct s_newton*)malloc(d*sizeof(struct s_newton));
	if (!N) {
		free((void*)s_newton_bij);
		return ai;
	}

	N[0].lead = 0u;
	N[0].t_deg = 0;
	N[0].sum = NULL;
	N[0].n_sum = 0;

	M = s_matrix_malloc(d, memory);
	if (!M) {
		freeN(N, 0, d);
		free((void*)s_newton_bij);
		return ai;
	}

	long i = 0;
	for (i = 1; i < d; ++i) {
		current = next_monomial(current, n_var);
		quo_monomial(&u, &quo, current, n_var);
		v = s_newton_bij[(unsigned long)quo];

		N[i].lead = current;
		N[i].id = -1;
		N[i].var = u;
		N[i].right = v;
		N[i].t_deg = N[v].t_deg + 1;
		N[i].sum = NULL;
		N[i].n_sum = 0;

		lo = s_matrix_formula(v);
		for (long j = v; j < i; j++) {
			if (s_newton_eval(N, i, elems, j, M, lo) == 1u) {
				s_newton_update(N, i, j);
			}

			lo = siouks_alg_next_lo(lo, j);
		}

		N[i].id = i;
		lo = s_matrix_formula(i);
		bool should_continue = false;
		for (j = i; j < d; ++j) {
			if (s_newton_eval(N, i, elems, j, M, lo) == 1u) {
				siouks_alg_xchg(M, elems, i, j);
				should_continue = true;
				break;

			}
			lo = siouks_alg_next_lo(lo, j);
		}

		if (should_continue) {
			N[i].id = i;
			s_newton_bij[(unsigned long)current] = i;
		}
		else {
			ai = N[i].t_deg;
			freeN(N, i, d);
			freeM(M);
			free((void*)s_newton_bij);

			return ai;
		}
	}

	ai = hamming(next_monomial(current, n_var));

	freeN(N, i, d);
	freeM(M);
	free((void*)s_newton_bij);

	return ai;
}