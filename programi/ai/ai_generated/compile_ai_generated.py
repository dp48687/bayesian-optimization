from distutils.core import setup, Extension


# We have to execute this module using command:
#
#       python3 compile.py build
#
# and then all .c files get compiled and we can then
# import those compiled files and use them in our python
# scripts.


COMPILED_FILE = 'ai_generated_compiled'
SOURCE_FILE_NAME = 'ai_generated.cpp'


if __name__ == "__main__":
    module = Extension(COMPILED_FILE, sources=[SOURCE_FILE_NAME])
    setup(version='1.0', description='A dummy description.', ext_modules=[module])
