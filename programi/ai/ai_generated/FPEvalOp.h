#ifndef EvalOp_h
#define EvalOp_h

#include <cmath>
#include <list>

class EvalOp : public EvaluateOp 
{
protected:
	uint nVariables;

public:
	FitnessP evaluate(IndividualP individual);
	void registerParameters(StateP);
	bool initialize(StateP);
	void Binary_FP_to_INT(IndividualP individual, double numOfFPvar, double Interval, uint id);
	std::list<string> from_DEC_to_BIN(double decode);
	string ConvertToBinary(int n, double dec);
	void BinaryTruthTable(std::vector<int>& tt, uint vel_TT, uint numFPvar, uint dekod);
	void GrayTruthTable(std::vector<int>& bin, uint vel_TT, uint numFPvar, uint dekod);

	// zajednicke varijable
	uint TT_size;
	uint FP_var;
	uint decode_by;
	string encode;
	string distribution;
	std::vector<uint> dec_ind;	// vektor cijelih brojeva dekodiran iz FP vektora

	StateP state_;
};
typedef boost::shared_ptr<EvalOp> EvalOpP;

#endif
