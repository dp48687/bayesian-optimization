#include <iostream>
#include <iomanip>
#include <fstream>
#include <ostream>
#include <cstring>
#include <string>
#include <map>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
//#include <thread>
//#include <mutex>
#include <queue>
#include <vector>
#include <limits.h>
#include <stdlib.h>



#define MON_SIZE    (long)(sizeof(unsigned int)*CHAR_BIT)
#define NB          (((long)sizeof(unsigned char)*CHAR_BIT) / 2)
#define WIDTH       (((long)sizeof(unsigned char)*CHAR_BIT) / NB)
#define MASK(n)      ((1u << n) - 1u)

struct elements {
	unsigned int  *e;
	long num_elms;
	long num_vars;
};

const unsigned long ulong_max = ULONG_MAX;
const unsigned char s_empty = ~((unsigned char)0u);
const unsigned char s_undef = 3u;

struct s_matrix {
	unsigned long n_coeff;
	unsigned long n_buf;
	unsigned long n;
	unsigned long alloc;
	unsigned char *coefficient;
	unsigned char *buffer;
};

struct s_newton {
	long id;
	long t_deg;
	long var;
	long right;
	unsigned int  lead;
	long n_sum;
	long *sum;
};

struct g_matrix {
	long n_coefficient;
	long n;
	unsigned char *coefficient;
};

struct g_newton {
	long id;
	long t_deg;
	long var;
	long right;
	long n_sum;
	long *sum;
};

unsigned int  calculate(unsigned int ones_indexes[], unsigned int n_vars);
unsigned int  siouksAlgorithm(struct elements *elems, unsigned long memory);
struct elements* getFiberOver1(unsigned int ones_indexes[], unsigned int n_vars);
unsigned int  evaluate(unsigned int ones_indexes[], long x);
void* adjustMemory(void *ptr, size_t size);
unsigned int  evaluateMonomial(unsigned int m, unsigned int x);
void sortElements(struct elements *elems);
int monomial_compare(const void *x, const void *y);
long hamming(unsigned int x);
struct s_matrix *s_matrix_malloc(long n, long mem);
long s_matrix_ineq(long M);
long safe_s_matrix_formula(long i);
unsigned int  saff_eval(long var, unsigned int a);
unsigned long safe_add(unsigned long a, unsigned long b);
unsigned long safe_sub(unsigned long a, unsigned long b);
unsigned long safe_mul(unsigned long a, unsigned long b);
unsigned long safe_div(unsigned long a, unsigned long b);
unsigned long safe_sum(unsigned long n);
long s_matrix_formula(long i);
unsigned long sum(unsigned long long n);
void freeN(g_newton* N, long i, long d);
void freeN(s_newton* N, long i, long d);
void freeM(s_matrix *M);
unsigned int next_monomial(unsigned int mo, long n_var);
void quo_monomial(long *var, unsigned int *r, unsigned int a, long n_var);
unsigned int s_newton_eval(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo);
long siouks_alg_next_lo(long lo, long i);
unsigned int s_newton_eval_buf(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M);
unsigned int s_newton_eval_void(struct s_newton *N, long Nid, struct elements *elems, long ptid);
unsigned int s_newton_eval_mem(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo);
void s_newton_update(struct s_newton *N, long Nid, long j);
void siouks_alg_xchg(struct s_matrix *M, struct elements *elms, long i, long j);
unsigned int s_newton_eval2_buf(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M);
unsigned int s_newton_eval2_void(struct s_newton *N, long Nid, struct elements *elems, long ptid);
unsigned int s_newton_eval_void(struct s_newton *N, long Nid, struct elements *elems, long ptid);
unsigned int s_newton_eval_mem(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo);
unsigned int s_newton_eval2_mem(struct s_newton *N, long Nid, struct elements *elems, long ptid, struct s_matrix *M, long lo);