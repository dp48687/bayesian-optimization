// https://yizhang82.dev/python-interop-capi - this is version for python 2.x
//
// info:
// https://docs.python.org/3/extending/extending.html
// https://stackoverflow.com/questions/24226001/importerror-dynamic-module-does-not-define-init-function-initfizzbuzz


#define PY_SSIZE_T_CLEAN
#include<Python.h>
#include<stdio.h>
#include "AI.cpp"


// How should we call the method below from Python:
// method(((arg1, arg2), (arg3, arg4)), (arg5, arg6))
// identifiers:
// i - integer
// s - string
// l - long int
// k - unisgned long
// L - long long
// K - unsigned long long
// f - float
// d - doubles
//
// more: https://docs.python.org/3/c-api/arg.html#arg-parsing


static PyObject* py_minimize_ANF(PyObject* self, PyObject* args) {
	
	// Write your implementation here.
    // int *tt, int n, unsigned int* anfpos

	PyObject* seq1;
	PyObject* seq2;
    int n;
    uint res;

    int *tt;
    int tt_length;

    unsigned int* anfpos;
    int anfpos_length;

    int i, result;

    /* get one argument as a sequence */
    if(!PyArg_ParseTuple(args, "(OiO)", &seq1, &n, &seq2)) {
        return 0;
    }

    seq1 = PySequence_Fast(seq1, "argument must be iterable");
    seq2 = PySequence_Fast(seq2, "argument must be iterable");
    if(!seq1 || !seq2) {
        return 0;
    }

    tt_length = PySequence_Fast_GET_SIZE(seq1);
    tt = (int*) malloc(tt_length * sizeof(int));
    if(!tt) {
        Py_DECREF(seq1);
        return PyErr_NoMemory();
    }
    for(i = 0; i < tt_length; ++i) {
        PyObject *fitem;
        PyObject *item = PySequence_Fast_GET_ITEM(seq1, i);
        if(!item) {
            Py_DECREF(seq1);
            free(tt);
            return 0;
        }
        fitem = PyNumber_Long(item);
        if(!fitem) {
            Py_DECREF(seq1);
            free(tt);
            PyErr_SetString(PyExc_TypeError, "All items must be numbers!");
            return 0;
        }
        tt[i] = PyLong_AS_LONG(fitem);
        Py_DECREF(fitem);
    }

    anfpos_length = PySequence_Fast_GET_SIZE(seq2);
    anfpos = (unsigned int*) malloc(anfpos_length * sizeof(unsigned int*));
    if(!anfpos) {
        Py_DECREF(seq2);
        return PyErr_NoMemory();
    }
    for(i = 0; i < anfpos_length; ++i) {
        PyObject *fitem;
        PyObject *item = PySequence_Fast_GET_ITEM(seq2, i);
        if(!item) {
            Py_DECREF(seq2);
            free(anfpos);
            return 0;
        }
        fitem = PyNumber_Long(item);
        if(!fitem) {
            Py_DECREF(seq2);
            free(anfpos);
            PyErr_SetString(PyExc_TypeError, "All items must be numbers!");
            return 0;
        }
        anfpos[i] = PyLong_AS_LONG(fitem);
        Py_DECREF(fitem);
    }

    // clean up, compute, and return result
    Py_DECREF(seq1);
    Py_DECREF(seq2);

    result = minimizeANF(tt, n, anfpos);
    res = calculate(anfpos, n);
    free(anfpos);
    free(tt);
    return PyLong_FromUnsignedLong(res);
}

static PyObject* py_calculate(uint ones_indexes[], uint n_vars) {

    return PyLong_FromUnsignedLong(result);
}


static PyMethodDef method_pool[] = {
	{"py_minimize_ANF", py_minimize_ANF, METH_VARARGS, "Method py_minimize_ANF."},
	{"py_calculate", py_minimize_ANF, METH_VARARGS, "Method py_calculate."},
    {NULL, NULL, 0, NULL}
};


static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT,
    "ai_generated_compiled",
    "NULL",
    -1,
    method_pool
};


PyMODINIT_FUNC PyInit_ai_generated_compiled(void)
{
    return PyModule_Create(&module);
}