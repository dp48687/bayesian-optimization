import ai.ai_generated.ai_generated_compiled as imported


def calc(tt, n, anf):
	"""

	:param tt:
	:param n:
	:param anf:
	:return:
	"""
	return imported.py_minimize_ANF(
		(
			tt, n, anf
		)
	)
