#include <ecf/ECF.h>
#include "FPEvalOp.h"
#include <list>
#include <algorithm>


// extern functions from boolean.cpp
extern int minimizeANF (int *tt, int n);


#define CHECK_BIT(var, pos) ((var)& (1 << (pos)))


void EvalOp::registerParameters(StateP state)
{
	state->getRegistry()->registerEntry("bool_variables", (voidP)(new uint(4)), ECF::UINT);

	// parametar za dekodiranje realne vrijednosti u niz bitova: "binary" - binarno, "gray" - Grayev kod
	state->getRegistry()->registerEntry("encoding", (voidP)(new std::string("binary")), ECF::STRING);
	state->getRegistry()->registerEntry("distribution_of_bits", (voidP)(new std::string("concatenation")), ECF::STRING);
}


bool EvalOp::initialize(StateP state)
{
	state_ = state;

	voidP varp = state->getRegistry()->getEntry("bool_variables");
	nVariables = *((uint*)varp.get());		// number of boolean variables

	voidP enc = state->getRegistry()->getEntry("encoding");
	encode = *((string*)enc.get());
	if (encode != "binary" && encode != "gray") {
		throw std::string("Error: -encoding- can be only binary or gray!");
	}

	voidP conc = state->getRegistry()->getEntry("distribution_of_bits");
	distribution = *((string*)conc.get());

	// zajednicka inicijalizacija
	TT_size = (uint)(pow(2., (int) nVariables)); // size of the truth table for nVariables 
	FloatingPoint::FloatingPoint* fp = (FloatingPoint::FloatingPoint*) state->getPopulation()->getLocalDeme()->at(0)->getGenotype().get();
	FP_var = (uint)fp->realValue.size();		// dimension of floatingpoint - number of variables

	// prijaviti gresku ako ovo gore nije djeljivo
	if (TT_size%FP_var != 0) {
		throw std::string("Error: size of the truth table should be a multiple of a dimension of floatingpoint! ");
	}
	decode_by = TT_size / FP_var;		// one FP value equals decode_by number of bits
	dec_ind.resize(FP_var);

	std::cout << "Broj FP varijabli je " << FP_var << "\n";
	std::cout << "Broj Bool varijabli je " << nVariables << "\n";
	std::cout << "Velicina tablice istinitosti je " << TT_size << "\n";
	std::cout << "Decode_by je " << decode_by << "\n";

	return true;
}


// pretvori vektor FP brojeva u vektor prirodnih brojeva
void EvalOp::Binary_FP_to_INT(IndividualP individual, double numOfFPvar, double interval, uint id = 0) 
{
	FloatingPoint::FloatingPoint* genf = (FloatingPoint::FloatingPoint*) individual->getGenotype(id).get();

	for (uint i = 0; i < numOfFPvar; i++){		// po broju varijabli
		uint broj = floor(genf->realValue[i] / interval );
		dec_ind[i] = broj;
	}
	
}


// funkcija koja daje tablicu istinitosti u obliku std::vector<uint> (za binarno kodiranje)
void EvalOp::BinaryTruthTable(std::vector<int>& tt, uint vel_TT, uint numFPvar, uint dekod)
{
	tt.assign(vel_TT, 0);
	
	uint k = 0;
	for (uint i = numFPvar-1; i >= 0; i = i - 1){		
		int broj = dec_ind[i];
		for (uint j = 0; j < dekod; j++) {
			if (broj % 2 == 1) {
				broj = broj / 2;
				tt[k] = 1;
				k = k + 1;
			}
			else {
				broj = broj / 2;
				tt[k] = 0;
				k = k + 1;
			}
			
		}
		if (k == vel_TT) break;
	}
	reverse(tt.begin(), tt.end());
}


// funkcija koja pretvara u Grayevo kodiranje
void EvalOp::GrayTruthTable(std::vector<int>& bin, uint vel_TT, uint numFPvar, uint dekod) {
	std::vector<int> gray, pom;
	gray.resize(vel_TT);
	pom.resize(dekod);
	for (uint i = 0; i < bin.size(); i+=dekod) {
		//std::cout << i;
		for (uint j = 0; j < dekod; j++) {
			if (i + j == dekod + i - 1) {
				pom[j] = 0;
			}
			else {
				pom[j] = bin[i + j + 1];
			}
			//std::cout << bin[i + j];
			//std::cout << pom[j];
			if (bin[i + j] == pom[j]) {
				gray[i + j] = 0;
			}
			else if (bin[i + j] != pom[j]) {
				gray[i + j] = 1;
			}
		}
	}
	reverse(gray.begin(), gray.end());
	bin = gray;
}


FitnessP EvalOp::evaluate(IndividualP individual)
{
	FitnessP fitness(new FitnessMax);

	FloatingPoint::FloatingPoint* gen = (FloatingPoint::FloatingPoint*) individual->getGenotype().get();

	double rezz;
	std::vector<int> nova_tablica;

	double interval;
	interval = 1 / pow(2., (int)decode_by);
	//std::cout << "interval: " << interval;

	//turns floating-point into integers
	Binary_FP_to_INT(individual, FP_var, interval);

	std::vector<int> tablicaBin;

	BinaryTruthTable(tablicaBin, TT_size, FP_var, decode_by);
		
	if (encode == "gray") {
		reverse(tablicaBin.begin(), tablicaBin.end());
		GrayTruthTable(tablicaBin, TT_size, FP_var, decode_by);
	}

	if (distribution == "every_nth") {
			
		nova_tablica.resize(TT_size);
		int j = 0, k = 0, br = 0;
		for (int i = 0; i < tablicaBin.size(); i++) {
			if (br == decode_by) {
				k++;
				j = k;
				br = 0;
			}
			nova_tablica[j] = tablicaBin[i];
			j = j + FP_var;
			br++;
		}
		tablicaBin = nova_tablica;
	}


	rezz = eval(&tablicaBin[0], int(nVariables), iFunction_);

	fitness->setValue(rezz);
	return fitness;
}
