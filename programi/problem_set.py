import json
import os.path as pth

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.datasets import make_circles, make_moons
from sklearn.metrics import classification_report

from optimize import gradient_descent as gs
import sample_data
from bayesian_optimization import BayesOptModel

from ai.ai_generated import run_ai_generated as run_ai


RANDOM_STATE = 2019


def generate_easy_dataset(visualize=False):
    from sklearn.model_selection import train_test_split
    X, y = make_circles(n_samples=500, noise=0.08, factor=0.4, random_state=RANDOM_STATE)

    X1 = X.copy()
    X2 = X.copy() + np.array([2, 1])
    X = np.concatenate((X1, X2))
    y = np.concatenate((y, y))

    if visualize:
        V = np.transpose(X)
        for i in range(len(V[0])):
            plt.scatter(V[0][i], V[1][i], c="#FF0000" if y[i] else "#0000FF", s=8)

        plt.show()

    return train_test_split(X, y, test_size=0.25, random_state=RANDOM_STATE)


def generate_hard_dataset(info=None, visualize=False):
    """

    :param info:
    :param visualize:
    :return:
    """

    info = (
            (3, -1, 300, 0.1, 0.5, "circles"),
            (6, -1, 300, 0.2, 0.1, "circles"),
            (3, -3, 300, 0.07, 0.5, "circles"),
            (6, -3, 300, 0.09, 0.5, "circles"),
            (4.5, 1, 200, 0.1, 0.6, "circles", "invert labels"),
            (4.5, -5, 200, 0.2, 0.2, "circles", "invert labels"),
            (4.5, -2, 200, 0.01, 0.5, "circles")
    ) if info is None else info

    from sklearn.model_selection import train_test_split
    data_X = []
    data_y = []
    for info_chunk in info:
        if "circles" in info_chunk:
            X, y = make_circles(n_samples=info_chunk[2],
                                noise=info_chunk[3],
                                factor=info_chunk[4],
                                random_state=RANDOM_STATE)
        else:
            X, y = make_moons(n_samples=info_chunk[2],
                                noise=info_chunk[3],
                                random_state=RANDOM_STATE)
        data_X.append(X.copy() + [info_chunk[0], info_chunk[1]])
        data_y.append(y if "invert labels" not in info_chunk else ((y - 1) * -1))
    X = np.concatenate(tuple(data_X))
    y = np.concatenate(tuple(data_y))

    if visualize:
        V = np.transpose(X)
        for i in range(len(V[0])):
            plt.scatter(V[0][i], V[1][i], c="#FF0000" if y[i] else "#0000FF", s=8)

        plt.show()

    return train_test_split(X, y, test_size=0.25, random_state=RANDOM_STATE)


def process_problem(func, info, n_iter=5, show=False, print_result_after_every_iter=False, save_to_file=False):
    opt_model = BayesOptModel(func, info)

    the_best_values = []

    for i in range(n_iter):
        accumulated_points = opt_model.minimize()
        print(accumulated_points)
        k_best, v_best = extract_the_best(accumulated_points)
        if print_result_after_every_iter:
            print(f"ITERATION {i}:   argmin: {k_best}   value(argmin) = {v_best}")
        the_best_values.append((k_best, v_best))
        if show:
            opt_model.show_results()

    return the_best_values


def extract_the_best(results):
    the_best_arg = None
    the_best_val = None
    for arg in list(results.keys()):
        if the_best_arg is None or results[arg] < the_best_val:
            the_best_arg, the_best_val = arg, results[arg]
    return the_best_arg, the_best_val


def problem_1(n_iter=5):
    """
    Minimization of function f(x) = sum{e^-((x - mu_i)^2/sigma_i^2)}, i in {1, 2 ... 5}

    ***** argmin(acquisition function) ****************
    * Limited memory Broyden–Fletcher–Goldfarb–Shanno *
    ***************************************************

    :param n_iter:
    :return:
    """
    results = process_problem(sample_data.gaussian_sum,
                    json.load(open(pth.join("info", "info_1.json"))),
                    show=True)


def problem_2(n_iter=5):
    """
    No gradient descent!
    :param n_iter:
    :return:
    """
    results = process_problem(sample_data.gaussian_sum_2d,
                              json.load(open(pth.join("info", "info_2.json"))),
                              show=False)
    print(results)


def problem_2_gd():
    """
    With gradient descent (20 iterations).
    :return:
    """
    local_history = {}


    def goal_function(x1, x2):
        generated = gs.gradient_descent(
            sample_data.gaussian_sum_2d,
            sample_data.gaussian_sum_2d_deriv,
            np.array([x1, x2])
        )

        local_history[f"{generated}"] = sample_data.gaussian_sum_2d(*generated)
        return sample_data.gaussian_sum_2d(*generated)

    results = process_problem(goal_function,
                              json.load(open(pth.join("info", "info_4.json"))),
                              n_iter=20,
                              show=False)
    print(f"\n\n\n\nFINISHED, final results: \n\n{local_history}")


def problem_5():
    """
    Optimization of number of neurons in two hidden layers and initial learning rate.
    :return:
    """
    X_train, X_test, y_train, y_test = generate_easy_dataset()

    def to_minimize(x1, x2, x3):
        mlp = MLPClassifier(
            hidden_layer_sizes=(int(x1), int(x2)),
            max_iter=100,
            activation='logistic',
            solver='adam',
            # random_state=RANDOM_STATE,
            learning_rate_init = max(x3 / 10000.0, 0.0001)
        )
        mlp = mlp.fit(X_train, y_train)
        results = mlp.predict(X_test)
        classification_loss = 0
        for i in range(len(y_test)):
            if results[i] != y_test[i]:
                classification_loss += 1
        return classification_loss / float(len(y_test))

    results = process_problem(
        to_minimize,
        json.load(open(pth.join("info", "info_5.json"))),
        show=False,
        n_iter=20,
        print_result_after_every_iter=True
    )


def problem_6():
    # ann => easy dataset
    """
        Optimization of number of neurons in 2-layer network, initial learning rate, activation function
        and optimization algorithm.
        :return:
    """

    X_train, X_test, y_train, y_test = generate_easy_dataset(False)

    def to_minimize(x1, x2, x3, x4):
        solver = int(x4)
        if solver == 0:
            solver = 'adam'
        elif solver == 1:
            solver = 'sgd'
        else:
            solver = 'lbfgs'

        mlp = MLPClassifier(
            hidden_layer_sizes=(int(x1), int(x2)),
            max_iter=100,
            activation='logistic',
            solver=solver,
            # random_state=RANDOM_STATE,
            learning_rate_init=max(x3 / 10000.0, 0.000001)
        )

        fitted = mlp.fit(X_train, y_train)
        results = fitted.predict(X_test)
        classification_loss = 0
        for i in range(len(y_test)):
            if results[i] != y_test[i]:
                classification_loss += 1
        return classification_loss / float(len(y_test))

    results = process_problem(
        to_minimize,
        json.load(open(pth.join("info", "info_6.json"))),
        show=False,
        n_iter=100,
        print_result_after_every_iter=True
    )


def problem_7():
    """
    Problem 6 with harder dataset.
    :return:
    """
    X_train, X_test, y_train, y_test = generate_hard_dataset()

    def to_minimize(x1, x2, x3, x4, x5):
        solver = int(x5)
        if solver == 0:
            solver = 'adam'
        elif solver == 1:
            solver = 'sgd'
        else:
            solver = 'lbfgs'
        mlp = MLPClassifier(
            hidden_layer_sizes=(int(x1), int(x2), int(x3)),
            max_iter=2000,
            activation='logistic',
            solver=solver,
            # random_state=RANDOM_STATE,
            learning_rate_init=max(x4 / 10000.0, 0.000001)
        )
        mlp = mlp.fit(X_train, y_train)
        results = mlp.predict(X_test)
        classification_loss = 0
        for i in range(len(y_test)):
            if results[i] != y_test[i]:
                classification_loss += 1
        return classification_loss / float(len(y_test))

    results = process_problem(
        to_minimize,
        json.load(open(pth.join("info", "info_7.json"))),
        show=False,
        n_iter=1000,
        print_result_after_every_iter=True
    )


def problem_8():
    # svm => harder dataset
    # we are optimizing kernel type, C, degree of polynomial kernel, gamma, coef0
    kernels = (
        'poly',
        'rbf',
        'sigmoid'
    )
    X_train, X_test, y_train, y_test = generate_hard_dataset()
    def to_minimize(x1, x2, x3, x4, x5):
        classifier = SVC(
            C=x2/10000.0,
            kernel=kernels[min(int(x1), len(kernels) - 1)],
            degree=int(x3),
            gamma=x4/1000.0,
            coef0=x5/1000.0,
            random_state=None
        )
        trained = classifier.fit(X_train, y_train)
        results = trained.predict(X_test)
        classification_loss = 0
        for i in range(len(y_test)):
            if results[i] != y_test[i]:
                classification_loss += 1
        return classification_loss / float(len(y_test))

    results = process_problem(
        to_minimize,
        json.load(open(pth.join("info", "info_8.json"))),
        show=False,
        n_iter=1000,
        print_result_after_every_iter=True
    )


def problem_9():

    def to_minimize(x1, x2, x3):
        return run_ai.calc(x1, x2, x3)

    results = process_problem(
        to_minimize,
        json.load(open(pth.join("info", "info_9.json"))),
        show=False,
        n_iter=1000,
        print_result_after_every_iter=True
    )


def problem_10():
    """
    Gaussian process classification.
    :return:
    """
    from sklearn.gaussian_process import GaussianProcessClassifier
    from sklearn.gaussian_process.kernels import RBF

    X_train, X_test, y_train, y_test = generate_hard_dataset()
    visualize_classifier_performance(GaussianProcessClassifier(kernel=1.0 * RBF(1.0), random_state=0),
                                     X_train, X_test, y_train, y_test)


def visualize_classifier_performance(model, X_train, X_test, y_train, y_test, ranges=(0, 9, -8, 4)):
    gpc = model.fit(X_test, y_test)


    print(classification_report(y_test, model.predict(X_test)))

    h = 0.2

    figure = plt.figure(figsize=(27, 9))
    i = 1
    xx, yy = np.meshgrid(np.arange(*ranges[0:2], h), np.arange(*ranges[2:], h))
    cm = plt.cm.RdBu
    cm_bright = ListedColormap(['#FF0000', '#0000FF'])
    ax = plt.subplot(1, 2, i)
    ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright, edgecolors='k')
    ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6, edgecolors='k')
    ax.set_xlim(xx.min(), xx.max())
    ax.set_ylim(yy.min(), yy.max())
    ax.set_xticks(())
    ax.set_yticks(())
    i += 1
    clf = gpc
    ax = plt.subplot(1, 2, i)

    if hasattr(clf, "decision_function"):
        Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    else:
        Z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
        Z = Z.reshape(xx.shape)
        ax.contourf(xx, yy, Z, cmap=cm, alpha=.8)
        ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright, edgecolors='k')
        ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, edgecolors='k', alpha=0.6)
        ax.set_xlim(xx.min(), xx.max())
        ax.set_ylim(yy.min(), yy.max())
        ax.set_xticks(())
        ax.set_yticks(())

    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    problem_10()
