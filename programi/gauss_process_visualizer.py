import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import LinAlgError

import sample_data


def gauss_kernel(x_i, x_j, gamma=1):
    return np.exp(-gamma * squared_euclidean_distances(x_i, x_j))


def inverse_quadratic_kernel(x_i, x_j, epsilon=1.5):
    return 1.0 / (1.0 + epsilon * epsilon * squared_euclidean_distances(x_i, x_j))


def squared_euclidean_distances(x_i, x_j):
    distances = [[0 for _ in range(len(x_j))] for _ in range(len(x_i))]
    for i, x in enumerate(x_i):
        for j, y in enumerate(x_j):
            distances[i][j] = (x[0] - y[0]) ** 2
    return np.array(distances)


def generate_vector(func, lb, ub, n, n_samples):
    vec = [[func(t) for t in np.linspace(lb, ub, n)] for _ in range(n_samples)]
    return np.transpose(np.array(vec))


def plot_gp_prior(kernel, mu_t=0):
    # source: http://katbailey.github.io/post/gaussian-processes-for-dummies/

    n = 100
    samples = np.linspace(-5, 5, n).reshape(-1, 1)

    cov_matrix = kernel(samples, samples)
    
    while True:
        try:
            lower_triangle_matrix = np.linalg.cholesky(cov_matrix)
            break
        except LinAlgError:
            print()

    mu = np.linspace(-5, 5, n).reshape(-1, 1) * 2.5
    f_prior = np.dot(lower_triangle_matrix, np.random.normal(size=(n, 3))) + mu_t
    plt.plot(samples, f_prior, linewidth=1.25)
    plt.show()


def plot_gp_posterior(kernel):
    # source: http://katbailey.github.io/post/gaussian-processes-for-dummies/

    n = 50
    samples = np.linspace(-5, 25, n).reshape(-1, 1)

    K_ss = kernel(samples, samples)

    # Noiseless training data
    Xtrain = np.array([-4, -3, -2, -1, 1]).reshape(5, 1)
    ytrain = sample_data.gaussian_sum(Xtrain)

    # Apply the kernel function to our training points
    K = kernel(Xtrain, Xtrain)
    L = np.linalg.cholesky(K)

    # Compute the mean at our test points.
    K_s = kernel(Xtrain, samples)
    Lk = np.linalg.solve(L, K_s)
    mu = np.dot(Lk.T, np.linalg.solve(L, ytrain)).reshape((n,))

    # Compute the standard deviation so we can plot it
    s2 = np.diag(K_ss) - np.sum(Lk ** 2, axis=0)
    stdv = np.sqrt(s2)
    # Draw samples from the posterior at our test points.
    L = np.linalg.cholesky(K_ss + 1e-6 * np.eye(n) - np.dot(Lk.T, Lk))
    f_post = mu.reshape(-1, 1) + np.dot(L, np.random.normal(size=(n, 3)))

    plt.plot(Xtrain, ytrain, 'bs', ms=8)
    plt.plot(samples, f_post)
    plt.gca().fill_between(samples.flat, mu - 2 * stdv, mu + 2 * stdv, color="#dddddd")
    plt.plot(samples, mu, 'r--', lw=2)
    # plt.axis([-5, 5, -3, 3])
    plt.title('Three samples from the GP posterior')
    plt.show()


def set_ticks_info(size=14):
    plt.xticks(fontsize=size)
    plt.yticks(fontsize=size)
    plt.locator_params(axis='y', nbins=5)
    plt.locator_params(axis='x', nbins=5)


if __name__ == "__main__":
    set_ticks_info()
    plot_gp_prior(inverse_quadratic_kernel, generate_vector(lambda t: 0.14*t * t * np.sin(2*t), -5, 5, 100, 3))
    set_ticks_info()
    plot_gp_prior(inverse_quadratic_kernel, generate_vector(lambda t: 0.5*t**2, -5, 5, 100, 3))
    set_ticks_info()
    plot_gp_prior(inverse_quadratic_kernel, generate_vector(lambda t: 0.1*np.exp(-t), -5, 5, 100, 3))
