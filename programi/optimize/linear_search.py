

EPSILON = 1E-5


def linear_search(func, a, b, precision=1E-5):
    the_best_func_val = func(a)
    the_best_argument = a

    current = a
    step = precision
    while current < b:
        evaluated = func(current)
        if evaluated < the_best_func_val:
            the_best_func_val = evaluated
            the_best_argument = current
        current += step
    return the_best_argument


if __name__ == "__main__":
    print(linear_search(lambda x: x*x - x, -10, 10, precision=0.0001))
