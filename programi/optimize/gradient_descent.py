import numpy as np

import optimize.functions as func
import optimize.linear_search as ls

EPSILON = 1E-5


def gradient_descent(f, gradient_func, x0, optimal_step_size=True, n_iter=120):
    """

    :param f:
    :param gradient_func:
    :param x0:
    :param optimal_step_size:
    :param n_iter:
    :return:
    """

    x = x0.copy()
    current_iteration = 0

    while current_iteration < n_iter:

        current_iteration += 1

        gradient = -gradient_func(*x)

        if optimal_step_size:
            f_lambda = func.lambda_func(f, x, gradient)
            eta = ls.linear_search(f_lambda, -10, 10, precision=0.01)
        else:
            eta = 1

        for i, grad_i in enumerate(gradient):
            x[i] += eta * gradient[i]

        if np.sum(np.power(eta * gradient, 2)) < EPSILON:
            break

    return x


if __name__ == "__main__":
    s = lambda x, y: x * x + y * y - 6
    s_deriv = lambda x, y: np.array([2 * x, 2 * y])
    print(gradient_descent(s, s_deriv, [2, 7]))
