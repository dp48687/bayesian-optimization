import math


MAX_ITERATIONS = 1000
EPSILON = 1E-5
k_inv = 2 / (math.sqrt(5) + 1)


def golden_section_search(func, a, c, b):
    c = a + (b-a) * k_inv if c is None else c
    if abs(a - b) < EPSILON:
        return (a + b) / 2.0
    d = c + k_inv * (b - c)
    return golden_section_search(func, c, d, b) if func(d) < func(c) else golden_section_search(func, d, c, a)


if __name__ == "__main__":
    print(golden_section_search(lambda x: x*x - x, -10, -10 + k_inv * 20, 10))
