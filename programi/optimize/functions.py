
def euclid_norm_squared(vector):
    result = 0
    for element in vector:
        result += element * element
    return result


def lambda_func(function_x, vector_, epsilon_vector):
    """
    Returns function f(L) = f(x + epsilon * L).
    Function which it accepts under the hood is f : Rn -> R
    :param function_x:
    :param vector_:
    :param epsilon_vector:
    :return:
    """

    def f_x_lambda_eps(lambda_eps_scalar):
        vector = []
        for i, element in enumerate(vector_):
            vector.append(element + lambda_eps_scalar * epsilon_vector[i])
        return function_x(*vector if len(vector) > 1 else vector[0])

    return f_x_lambda_eps
