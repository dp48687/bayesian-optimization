import warnings

import matplotlib.pyplot as plt
import numpy as np
from bayes_opt import BayesianOptimization as BayesOpt
from bayes_opt import UtilityFunction
from matplotlib import gridspec
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C

warnings.filterwarnings("ignore")


class BayesOptModel:

    def __init__(self, func_to_minimize, json_config):
        """

        :param func_to_minimize:
        :param json_config:
        """
        self.json_config = json_config
        alg = json_config["algorithm"]

        self.func = func_to_minimize
        self.var_names = alg["variable_names"]
        if not isinstance(self.var_names, list):
            self.var_names = eval(self.var_names)
        self.x_range = tuple(alg["domain_bounds"][self.var_names[0]])
        is_discrete = json_config["is_discrete"]
        self.kernel = C(1.0, (1e-3, 1e3), discrete=is_discrete) * RBF(10, (1e-2, 1e2), discrete=is_discrete)
        self.gp = GaussianProcessRegressor(kernel=self.kernel, n_restarts_optimizer=9)
        # the default gpr:
        # self.gp = GaussianProcessRegressor(
        #     kernel=Matern(nu=2.5),
        #     alpha=1e-6,
        #     normalize_y=True,
        #     n_restarts_optimizer=25,
        #     random_state=2019
        # )
        self.optimizer = BayesOpt(
            f=self.func,
            pbounds=alg["domain_bounds"],
            utility_function=eval(self.json_config["algorithm"]["utility_function"]["expression"]),
            random_state=0,
            verbose=0,
            init_points=alg["initial_points"],
            gp=self.gp,
            discrete=is_discrete
        )

    def minimize(self):
        """

        :return:
        """
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.optimizer.process_one_iteration()
        return self.optimizer._space._cache

    def show_results(self):
        v = Visualizer1D(self.json_config)
        x_range = np.linspace(*self.x_range, 10000).reshape(-1, 1)
        y_range = np.array([self.func(x_range)]).reshape(-1, 1)
        v.plot_gp(self.optimizer, self.var_names, x_range, y_range)
        plt.show()


# Kod za vizualizaciju preuzet sa
# https://github.com/fmfn/BayesianOptimization/blob/master/examples/visualization.ipynb

class Visualizer1D:

    def __init__(self, json_config):
        self.json_config = json_config

    def posterior(self, optimizer, x_obs, y_obs, grid):
        optimizer._gp.fit(x_obs, y_obs)
        mu, sigma = optimizer._gp.predict(grid, return_std=True)
        return mu, sigma

    def plot_gp(self, optimizer, var_names, x, y):
        fig = plt.figure(figsize=tuple(self.json_config["figure_size"]))

        gs = gridspec.GridSpec(2, 1, left=0.1, right=0.7, height_ratios=[2, 1])
        axis = plt.subplot(gs[0])
        acq = plt.subplot(gs[1])

        x_obs = np.array([[res["params"][var_names[0]]] for res in optimizer.res])
        y_obs = np.array([res["target"] for res in optimizer.res])

        mu, sigma = self.posterior(optimizer, x_obs, y_obs, x)
        axis.plot(x, y, linewidth=2, label='Goal function')
        axis.plot(x, mu, '--', color='k', label=f'$\mu({var_names[0]})$')
        axis.plot(x_obs.flatten(), y_obs, 'D', markersize=6, label=u'Observations', color='r')
        axis.fill(np.concatenate([x, x[::-1]]),
                  np.concatenate([mu - 1.9600 * sigma, (mu + 1.9600 * sigma)[::-1]]),
                  alpha=.2, fc='c', ec='None', label='95% confidence interval')
        axis.set_xlim(tuple(self.json_config["algorithm"]["goal_function"]["x_bounds"]))

        u_func = eval(self.json_config["algorithm"]["utility_function"]["expression"])
        utility = u_func.utility(x, optimizer._gp, 0)
        acq.plot(x, utility, label='Utility Function', color='purple')
        acq.plot(x[np.argmin(utility)], utility[np.argmin(utility)], 'D', markersize=5,
                 label=u'Next Best Guess', markerfacecolor='gold', markeredgecolor='k', markeredgewidth=1)
        acq.set_xlim(tuple(self.json_config["algorithm"]["utility_function"]["x_bounds"]))
        acq.set_ylim((np.min(utility) - 2, np.max(utility) + 2))

        axis.legend(loc=2, bbox_to_anchor=(1.01, 1))
        acq.legend(loc=2, bbox_to_anchor=(1.01, 1))
