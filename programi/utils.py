import numpy as np
from numpy.linalg import LinAlgError
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy import optimize
import sample_data
import random


def plot_bivariant_distribution(surface=False, mu=None, cov=None):
    """
    Source: https://scipython.com/blog/visualizing-the-bivariate-gaussian-distribution/
    Draws a bivariate Gaussian distribution with expectancy vector (0, 1)^T and
    covariance matrix [[1.2, -0.5], [-0.5, 1]]
    :param surface: if True, it plots just function contours, otherwise it draws 3D function
    :return:
    """
    N = 100
    X = np.linspace(0, 12, N)
    Y = np.linspace(-4, 4, N)
    X, Y = np.meshgrid(X, Y)

    mu = np.array([5, 0]) if mu is None else mu
    sigma = np.array([[2, 1.445], [1.445, 2]]) if cov is None else cov

    pos = np.empty(X.shape + (2,))
    pos[:, :, 0] = X
    pos[:, :, 1] = Y

    def multivariate_gaussian(pos, mu, Sigma):
        n = mu.shape[0]
        sigma_det = np.linalg.det(Sigma)
        sigma_inv = np.linalg.inv(Sigma)
        N = np.sqrt((2 * np.pi) ** n * sigma_det)
        fac = np.einsum('...k,kl,...l->...', pos - mu, sigma_inv, pos - mu)

        return np.exp(-fac / 2) / N

    Z = multivariate_gaussian(pos, mu, sigma)

    if surface:
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        set_ticks_info(ax)
        ax.plot_surface(X, Y, Z, rstride=1, cstride=1, linewidth=2, antialiased=True, cmap=cm.rainbow)
    else:
        # cset = ax.contourf(X, Y, Z, zdir='z', offset=0, cmap=cm.coolwarm)
        fig = plt.figure()
        ax = fig.gca()
        set_ticks_info(ax)
        ax.contour(X, Y, Z, levels=8, linewidths=1, cmap=cm.seismic)
        plt.plot(np.linspace(6, 6, N), Y, color="#000000", linewidth=1)

    plt.show()


def plot_1d_gaussian(mean=2.78, variance=1.95):

    mean = 2.78 if mean is None else mean
    variance = 1.95 if variance is None else variance

    def gauss(arg, mu, sigma_sq):
        return np.exp(-np.square(arg - mu) / (2 * sigma_sq)) / np.sqrt(2 * np.pi * sigma_sq)

    draw_over = np.arange(-5, 9, .01)
    plt.plot(draw_over, np.array([gauss(i, mean, variance) for i in draw_over]))

    confidence95 = np.linspace(0.05, 5.51, 1000)
    plt.fill_between(confidence95, np.array([gauss(i, mean, variance) for i in confidence95]), color="#9fc2f9")
    plt.xlabel('f', size=15)
    plt.ylabel('$\\mathcal{N}\;(2.78, 1.95)$', size=15)
    plt.axis([-5, 9, 0, 0.4])
    plt.show()


def set_ticks_info(axes=None, size=14, labelx="$f$", labely="$f^{\:\:'}$"):
    plt.locator_params(axis='y', nbins=5)
    plt.locator_params(axis='x', nbins=6)
    if axes is not None:
        axes.tick_params(labelsize=size)
        axes.tick_params(labelsize=size)
    else:
        plt.tick_params(labelsize=size)
        plt.tick_params(labelsize=size)
    if labelx is not None:
        plt.xlabel(labelx, rotation=0, size=15, labelpad=5)
    if labely is not None:
        plt.ylabel(labely, rotation=0, size=15, labelpad=5)


def plot_2d_function(func, levels=25):
    X = np.linspace(-5, 5, 1000)
    Y = np.linspace(-5, 5, 1000)
    X, Y = np.meshgrid(X, Y)
    Z = func(X, Y)

    fig = plt.figure()
    ax = fig.gca()
    set_ticks_info(ax, labelx="$x_1$", labely="$x_2$")
    #ax.contour(X, Y, Z, levels=8, linewidths=1, cmap=cm.seismic)
    cset = ax.contourf(X, Y, Z, zdir='z', levels=levels, offset=0, cmap=cm.coolwarm)

    plt.show()


if __name__ == "__main__":
    random_x = [random.uniform(0, 5) for _ in range(0, 10)]
    random_y = [random.uniform(0, 5) for _ in range(0, 10)]
    import matplotlib.pyplot as plt

    plt.scatter(random_x, random_y)
    plt.show()
